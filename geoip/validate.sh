#!/bin/bash
# Confirm the validity of the GeoIP test specs after recent library updates
lockfile_app='geoip_tests'

# Stamp we're starting
echo "INSERT INTO LOCKFILE_STAMPS (app, last_started) VALUES ('$lockfile_app', NOW()) ON DUPLICATE KEY UPDATE last_started = VALUES(last_started);" | mysql debearco_admin

# Ensure the tests are run against the latest version (in the SysMon app, which we know needs it)
cd /var/www/debear/sites/sysmon
tests/PHPUnit/Setup/30-CI-CI_FAUX-CI_DEV-geoip.sh >/dev/null 2>&1
ret_code=$?
if [ $ret_code -ne 0 ]
then
    # The tests failed, so abort and pass on the return code
    exit $ret_code
fi

# Then run the tests
vendor/bin/phpunit --configuration tests/PHPUnit/00_core.xml tests/PHPUnit/App/Unit/GeoIPScriptsTest.php >/dev/null 2>&1
ret_code=$?
if [ $ret_code -ne 0 ]
then
    # The tests failed, so abort and pass on the return code
    exit $ret_code
fi

# Stamp we've completed
echo "INSERT INTO LOCKFILE_STAMPS (app, last_finished) VALUES ('$lockfile_app', NOW()) ON DUPLICATE KEY UPDATE last_finished = VALUES(last_finished);" | mysql debearco_admin
exit 0
