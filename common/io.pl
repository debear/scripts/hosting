# Input/Output-related functionality (files, etc)

#
# Save output to a file
#
sub output_file {
  my ($file, $msg) = @_;

  open FILE, "| gzip >>$file";
  binmode FILE, ":encoding(utf8)";
  print FILE $msg;
  close FILE;
}

# Return true to pacify the compiler
1;
