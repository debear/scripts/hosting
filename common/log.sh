# Log file methods (Bash version of Perl scripts)

# Add a message to the log list
exec 2>&1
function log() {
    echo "$(date +'[%F %T]') $1" | gzip >>$log_file
}
