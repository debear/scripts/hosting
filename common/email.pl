# Email-related functionality
use DBI;

#
# Send an email (internally)
#
sub send_email_queued {
  my ($app, $reason, $subject, $body) = @_;

  # Connect to the database
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'});
  $dbh->do('SET time_zone = "Europe/London";');

  # Build our query
  my $sql = 'INSERT INTO COMMS_EMAIL (app, email_id, email_reason, email_from, email_to, email_subject, email_body, email_queued, email_send, email_status)
    VALUES (?, NULL, ?, ?, ?, ?, ?, NOW(), NOW(), \'queued\');';

  # Process
  my $sth = $dbh->prepare($sql);
  $sth->execute($app, # email_app
                $reason, # email_reason
                $config{'email'}{'from'}, # email_from
                $config{'email'}{'to'}, # email_to
                $subject, # email_subject
                $body); # email_body

  # Disconnect
  $dbh->disconnect if defined($dbh);
}

# Return true to pacify the compiler
1;
