#!/usr/bin/perl -w
# Convert a database dump diff into DELETE/INSERT combo
# TD. 24/06/2014.
# TD. 08/04/2019. Convert DELETE->INSERT into INSERT INTO ... ON DUPLICATE KEY UPDATE statements
# TD. 15/01/2021. Remove requirement for table requiring a PRIMARY KEY; Improve debugging.

#
# BEGIN: Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;
  use Cwd 'abs_path';

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Mask the filename as we're on a shared server
  our $script_raw = $0;
  our $script = basename($0, '.pl');
  $0 = basename $0;

  # Some path customisation needs to be made if running on the prod server
  our $home_dir;
  our $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = abs_path(dirname(__FILE__) . '/..');
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $home_dir = $xilo_home;
  } else {
    $home_dir = '/var/www';
  }
}

use strict;
use MIME::Base64;
use DBI;

# Details on this table
my $db; my $table; my @pri_cols = (); my @tbl_cols = (); my $pri_key;

# Loop through command options
my %opt = ( );
for (my $i = 0; $i < @ARGV; $i++) {
  if (!defined($db)) {
    $db = $ARGV[$i];
  } elsif ($ARGV[$i] eq '--skip-ALTER') {
    $opt{'skip-alter'} = 1;
  } elsif ($ARGV[$i] eq '--skip-empty') {
    $opt{'skip-empty'} = 1;
  } elsif ($ARGV[$i] eq '--row-debug') {
    $opt{'row-debug'} = 1;
  }
}

# If no DIFFs, exit...
my @input = <STDIN>;
if (!@input) {
  print "# No difference in inputs...\n"
    if !$opt{'skip-empty'};
  exit;
}

my %sql = ( 'del' => [ ], 'ins' => [ ] );

#
# Pre-amble, using first line of diff
#
# Get the primary table details
parse_table_details($input[0]);
# Identify where these columns appear in the SQL
parse_table_cols($input[0]);

#
# Loop through and convert to SQL depending on the diff type
#
# Pass 1: Try and determine DELETE / INSERT / UPDATE (by primary key)
my %cmp = (
  'act' => {},
  'ins' => {},
  'del' => {},
);
foreach my $line (@input) {
  # What is it?
  chomp $line;
  my $diff = substr($line, 0, 1);
  my $sql = substr($line, 2);

  # Parse the values, getting the primary key components
  my @pri = ();
  my @values = parse_table_rows($sql);
  my ($values_str) = splice(@values, 0, 1);
  my @del_cols = (); # Whilst we're looping, build the DELETE statement... even if we won't necessarily use it
  foreach my $c (@pri_cols) {
    push @pri, defined($values[$c]) ? $values[$c] : 'NULL';
    # The DELETE line for later
    push @del_cols, (defined($values[$c]) ? "`$tbl_cols[$c]` = '$values[$c]'" : "`$tbl_cols[$c]` IS NULL");
  }
  my $pri = join(':@~', @pri);

  # What does this diff line represent?
  my $is_ins = ($diff eq '<');

  # Add to our action list and appropriate section
  if (!defined($cmp{'act'}{$pri})) {
    $cmp{'act'}{$pri} = { 'ins' => 0, 'del' => 0 };
  }
  $cmp{'act'}{$pri}{$is_ins ? 'ins' : 'del'} = 1;

  if ($is_ins) {
    $cmp{'ins'}{$pri} = $values_str;
  } else {
    $cmp{'del'}{$pri} = "DELETE FROM `$table` WHERE " . join(' AND ', @del_cols) . ';';
  }
}

# Pass 2: Determine what's an INSERT and what's a DELETE
foreach my $pri (sort keys %{$cmp{'act'}}) {
  # DELETE only
  if (!$cmp{'act'}{$pri}{'ins'} && $cmp{'act'}{$pri}{'del'}) {
    push @{$sql{'del'}}, $cmp{'del'}{$pri};
  # INSERT / UPDATE (treat the same via the ON DUPLICATE KEY UPDATE)
  } else {
    push @{$sql{'ins'}}, $cmp{'ins'}{$pri};
  }
}

#
# Output...
#
# Some optimisations
print "LOCK TABLES `$table` WRITE;\n";
print "ALTER TABLE `$table` DISABLE KEYS;\n";

# First, the DELETEs...
print join("\n", @{$sql{'del'}}) . "\n"
  if @{$sql{'del'}};

# Then the INSERTs (as potential UPDATEs)...
my @odku = ();
foreach my $col (@tbl_cols) {
  push @odku, "`$col` = VALUES(`$col`)";
}
while (@{$sql{'ins'}}) {
  my @splice = splice(@{$sql{'ins'}}, 0, 1000);
  print "INSERT INTO `$table` (`" . join('`, `', @tbl_cols) . "`) VALUES\n  (" . join("),\n  (", @splice) . ")\nON DUPLICATE KEY UPDATE " . join(', ', @odku) . ";\n";
}

# Finally, re-order...
if (!$opt{'skip-alter'}) {
  print "ALTER TABLE `" . $table . '` ORDER BY `';
  my @alt_cols = ( );
  foreach my $c (@pri_cols) {
    push @alt_cols, $tbl_cols[$c];
  }
  print join('`, `', @alt_cols) . "`;\n";
}

# Remove the optimisations
print "ALTER TABLE `$table` ENABLE KEYS;\n";
print "UNLOCK TABLES;\n";

#
# Identify the main details of this table
#
sub parse_table_details {
  my ($sql) = @_;
  ($table) = ($sql =~ m/INTO `([^`]+)` \(/si);

  # Password - raw, encoded, version from file
  our $home_dir;
  my $passwd_dir = $home_dir . '/debear/etc/passwd';
  my $db_pass = do { local(@ARGV, $/) = $passwd_dir . '/web/db'; <> }; chomp($db_pass);
  my $dbh = DBI->connect('dbi:mysql:information_schema', 'debearco_sysadmn', decode_base64($db_pass))
    or die("Unable to connect to the information_schema database: " . $DBI::errstr);
  ($pri_key) = $dbh->selectrow_array('SELECT CONCAT(",", GROUP_CONCAT(`COLUMN_NAME`), ",") AS pri FROM `KEY_COLUMN_USAGE` WHERE `CONSTRAINT_NAME` = "PRIMARY" AND `TABLE_SCHEMA` = ? AND `TABLE_NAME` = ?;', { }, $db, $table)
    or die("Unable to determine a primary key to $db.table: " . $DBI::errstr);
  $dbh->disconnect;
}

#
# Get where each (primary key) column appears in the list
#
sub parse_table_cols {
  my ($sql) = @_;
  my ($tmp) = ($sql =~ m/\(`([^\)]+)`\) VALUES \(/si);
  @tbl_cols = split('`, `', $tmp);
  my $i = 0;
  foreach my $col (@tbl_cols) {
    push @pri_cols, $i
      if !defined($pri_key) || $pri_key =~ m/,$col,/;
    $i++;
  }
}

#
# Split the INSERT query in to list of columns values
#
# Test:
# parse_table_rows("VALUES (9,'regular','with-baserunners','Man on 3rd, <2 Outs',0,233,NULL,'141,',7,\"asd\",'asd,''',',,''',''',''');"); exit;
sub parse_table_rows {
  my ($sql) = @_;
  my ($values) = ($sql =~ m/VALUES \((.+)\);\s*$/si);
  my @values = split(',', $values);
  my $delim = '';

  if ($opt{'row-debug'}) {
    print "Parse row\n=========\n- Split in to " . @values . " columns(s):\n";
    for (my $i = 0; $i < @values; $i++) {
      print "  - $i: $values[$i]\n";
    }
    print "- Parsing:\n";
  }
  for (my $i = 0; $i < @values; $i++) {
    print "  - $i: $values[$i]\n" if $opt{'row-debug'};
    # Double comma found in string
    if ($values[$i] eq '') {
      print "    - Empty string, merging comma in to previous element.\n" if $opt{'row-debug'};
      $values[$i - 1] .= ',';
      my $rmv = splice(@values, $i, 1);
      print "    - Reducing counter to reflect our decreased array size\n" if $opt{'row-debug'};
      $i--;
    # Searching for the start a delimited string?
    } elsif (!$delim) {
      my $start = substr($values[$i], 0, 1);
      if ($start eq "'" || $start eq '"') {
        $delim = $start;
        # But also strip from the value
        $values[$i] = substr($values[$i], 1);
        print "    - Found delimeter $delim, pruning from start of string\n" if $opt{'row-debug'};
        # Do we also end in the delimeter?
        if (!length($values[$i])) {
          print "    - Not checking end character, because no string remains\n" if $opt{'row-debug'};
        } elsif ($delim eq "'" && $values[$i] eq "''") {
          print "    - Not checking end character, because the string left is just an esaped '\n" if $opt{'row-debug'};
        } else {
          my $end = substr($values[$i], -1);
          if ($end eq $delim) {
            print "    - Delimeter $delim found at end, pruning from end of string\n" if $opt{'row-debug'};
            $values[$i] = substr($values[$i], 0, -1);
            # Reset the delimeter, as we've found what we're looking for in a single hit
            $delim = '';
          }
        }
      } elsif ($values[$i] eq 'NULL') {
        $values[$i] = undef;
        print "    - NULL value found\n" if $opt{'row-debug'};
      }
    # Searching for the end of a delimited string?
    } elsif (substr($values[$i], -1) eq $delim) {
      print "    - Found delimeter $delim at end, merging with previous value, pruning and resetting delimeter\n" if $opt{'row-debug'};
      # Concat to the previous element, removing the delimeter
      my $rmv = splice(@values, $i, 1);
      $values[$i - 1] .= ',' . substr($rmv, 0, -1);
      # Reset the delimeter, as we've found what we're looking for
      $delim = '';
      # Bounce back
      print "    - Reducing counter to reflect our decreased array size\n" if $opt{'row-debug'};
      $i--;
    }
  }

  # Return the whole string as the first argument
  if ($opt{'row-debug'}) {
    print "- Returning original value along with " . @values . " column(s)\n";
    print "  - Orig: $values\n";
    for (my $i = 0; $i < @values; $i++) {
      print "  - $i: " . (defined($values[$i]) ? $values[$i] : 'undef') . "\n";
    }
  }
  return ($values, @values);
}
