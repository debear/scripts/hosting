#!/bin/bash
# Automated table optimizer

# Config
if [ $(hostname | grep -c '\.xnoc\.net$') -eq 1 ]
then
  passwd=~
else
  passwd=/var/www
fi
passwd="$passwd/debear/etc/passwd/web/db"

# Database details
db_user=debearco_sysadmn
db_pass=`cat $passwd | base64 --decode`

# Command to run
mysqlcheck --all-databases --auto-repair --optimize --silent --user=$db_user --password=$db_pass
