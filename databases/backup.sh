#!/bin/bash
# Backup the databases
# TD. 2014-07-14.
#     2023-12-21. Reverted to a single script to take daily current-state dumps

#
# Base config
#
here=$(realpath $(dirname $0))
today=$(date +'%F')
db_backup=debearco_common

#
# Customisation
#
# Databases to ignore, argument to which will be inserted into a NOT IN (...) query
db_ignore="'debearco_sports'"

#
# Useful functions
#
# Logging...
exec 2>&1
function log() {
  echo $(date +'[%F %T]') "[$db] $1" | gzip >>$here/../../logs/common/backups/$today.log.gz
}

function log_status() {
  echo "INSERT INTO BACKUPS_LIST (db, last_$1) VALUES ('$db', NOW()) ON DUPLICATE KEY UPDATE last_$1 = VALUES(last_$1);" | mysql -s $db_backup
}

function log_starting() {
  log "Starting"
  log_status 'start'
}

function log_complete() {
  log_status 'end'
  ls -l *.tar >.status
  log "Completed"
}

#
# Start running...
#
for db in $(echo "SELECT SCHEMA_NAME FROM SCHEMATA WHERE SCHEMA_NAME LIKE 'debearco%' AND SCHEMA_NAME NOT IN ($db_ignore) ORDER BY SCHEMA_NAME;" | mysql -s information_schema); do
  # Does the backup structure exist?
  base_dir="$here/backups/$db/daily"
  if [ ! -e "$base_dir" ]; then
    mkdir -p $base_dir
  fi
  cd $base_dir

  # Skip if we've already processed today's backup
  if [ -e $today.tar ]; then
    log "Skipping - $today.tar already exists"
    continue
  fi
  log_starting

  # Proceed
  log "- Creating '$today' daily dir"
  mkdir -p $today

  # Treat each table individually
  for tbl in $(echo "SHOW TABLES;" | mysql -s $db); do
    log "- Dumping table '$tbl'"
    mysqldump --skip-column-statistics --skip-lock-tables --no-create-info --complete-insert --hex-blob --no-set-names --order-by-primary $db $tbl | grep '^INSERT INTO ' | gzip >$today/$tbl.sql.gz
  done
  log "- Collating daily backup"
  tar -cf $today.tar $today
  rm -rf $today

  # Prune anything over 7 days old, except those from 1st and 15th of each month
  log "- Pruning old backups"
  find $base_dir -ctime +7 | grep -Pv '\-(01|15).tar' | xargs -iBAK rm -f BAK

  # State that we're done!
  log_complete
done
