#!/bin/bash
# Archive "old" data within the database.
# TD. 2017-02-27.
#     2020-01-01. Instances to the database.

here=`dirname $0`

#
# Worker method
#
function archive() {
  # Re-name vars for readibility
  name=$1
  db=$2
  tbl=$3
  ttl=$4
  col=$5

  # Ensure our target save folder exists
  dst="$here/archives/$name"
  if [ ! -e $dst ]
  then
    mkdir -p $dst
  fi

  # Determine appropriate dates
  date_tbl=$tbl
  if [ "x$name" = 'xemails_links' ]
  then
    # Except the emails_links instance, when we want the dates from the email table instead...
    date_tbl=COMMS_EMAIL
    sec_join="CONCAT($date_tbl.app, ':', $date_tbl.email_id)"
    sec_using="app, email_id"
    sec_where="CONCAT(app, ':', email_id)"
  elif [ ! -z `echo "$name" | grep -P '^sysmon_reports_'` ]
  then
    # Or the sysmon_reports_* individual instances, where we want the dates from the report instance table instead...
    date_tbl=REPORTS_INSTANCE
    sec_join="$date_tbl.report_id"
    sec_using="report_id"
    sec_where="report_id"
  fi
  where="DATE($col) < DATE_SUB(CURDATE(), INTERVAL $ttl DAY) "
  dates=`echo "SELECT DISTINCT DATE($col) FROM $date_tbl WHERE $where ORDER BY $col;" | mysql -s $db`

  #
  # Date based (directly)
  #
  if [ $tbl = $date_tbl ]
  then
    # Loop through the dates, dumping these into files
    for date in ${dates[@]}
    do
      mysqldump $db $tbl --skip-column-statistics --no-create-info --complete-insert --skip-extended-insert --hex-blob --where="DATE($col) = '$date'" | gzip >>$dst/$date.sql.gz
    done

    # Delete from the database
    echo "DELETE FROM $tbl WHERE $where; OPTIMIZE TABLE $tbl;" | mysql -s $db >/dev/null

  #
  # Date comes from a secondary table
  #
  else
    # Determine appropriate IDs for each date
    for date in ${dates[@]}
    do
      num_left=`echo "SELECT COUNT(*) AS num_left FROM $date_tbl JOIN $tbl USING ($sec_using) WHERE DATE($col) = '$date';" | mysql -s $db`
      while [ $num_left -gt 0 ]
      do
        # Determine the next batch to use
        chunk=50
        ids=`echo "SET SESSION group_concat_max_len = 1048576; SELECT CONCAT(\"'\", GROUP_CONCAT(sq.id SEPARATOR \"','\"), \"'\") AS id_list FROM (SELECT DISTINCT $sec_join AS id FROM $date_tbl JOIN $tbl USING ($sec_using) WHERE DATE($col) = '$date' LIMIT $chunk) AS sq;" | mysql -s $db`
        where="$sec_where IN ($ids)"
        mysqldump $db $tbl --skip-column-statistics --no-create-info --complete-insert --skip-extended-insert --hex-blob --where="$where" | gzip >>$dst/$date.sql.gz
        echo "DELETE FROM $tbl WHERE $where;" | mysql -s $db >/dev/null

        # Re-count for our loop checker
        num_left=`echo "SELECT COUNT(*) AS num_left FROM $date_tbl JOIN $tbl USING ($sec_using) WHERE DATE($col) = '$date';" | mysql -s $db`
      done
      echo "OPTIMIZE TABLE $tbl;" | mysql -s $db >/dev/null
    done
  fi
}

# The rest are from the database
IFS=$'\n'
rules=$(echo "SELECT CONCAT(name, ',', db, ',', tbl, ',', ttl, ',', col) AS rules FROM ARCHIVE_RULES WHERE active = 1 ORDER BY \`order\`" | mysql -s debearco_admin)
for rule in ${rules[@]}
do
  # Split the arguments
  name=$(echo "$rule" | awk -F, '{print $1}')
  db=$(echo "$rule" | awk -F, '{print $2}')
  tbl=$(echo "$rule" | awk -F, '{print $3}')
  ttl=$(echo "$rule" | awk -F, '{print $4}')
  col=$(echo "$rule" | awk -F, '{print $5}')
  # Run
  archive $name $db $tbl $ttl $col
done
