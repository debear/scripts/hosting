#!/usr/bin/perl -w
# Download and import sports odds for upcoming games

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
  }
}

use strict;
use DateTime;
use LWP::UserAgent;
use Try::Tiny;
use JSON;
use List::Util qw/min max/;
use POSIX qw/floor/;
use HTML::Entities;

# Mask the filename as we're on a shared server
our $base_dir;
our $script_raw = $0;
$0 = basename $0;

# Get base config
our %config;
require $base_dir . '/config.pl';

# Logging info
setup_logging();

# Validate arguments
our %opt = (
  'scheduled' => 0, # Run in a 'scheduled' mode, which is to be at a configured time
  'no-save-log' => 0, # Display the log on STDOUT, not to file
);
foreach my $arg (@ARGV) {
  # Is it an option we're looking for?
  if (is_opt(\$arg)) {
    log_msg("* Running in '$arg' mode *");
    $opt{$arg} = 1;

  # Not expecting anything, so log we don't know what to do with the argument, and ignore it
  } else {
    log_msg("* Unknown argument '$arg'");
  }
}

# In scheduled mode, ensure we are running at the appropriate time
if ($opt{'scheduled'}) {
  my $dt_local = DateTime->from_epoch(epoch => time(), time_zone => 'UTC')->set_time_zone($config{'scheduled'}{'timezone'});
  my $hour_local = $dt_local->hour;
  if ($hour_local != $config{'scheduled'}{'hour'}) {
    my $now_local = sprintf('%02d:%02d', $hour_local, $dt_local->minute);
    log_msg("* Scheduled mode called outside the expected timeframe (Required: $config{'scheduled'}{'hour'}:xx; Currently: $now_local)");
    save_log();
    exit; # Silently abort
  }
}

# Create our LWP object that will interrogate the API
our $browser = new LWP::UserAgent;
$browser->agent('OddsSync/1.0');

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_COMMON_MAJORS_ODDS (sport, season, game_type, game_id, home_line, visitor_line, home_spread, visitor_spread, over_under, last_synced)
  VALUES (?, ?, ?, ?, ?, ?, ?, ?, IF(? = 1, ?, NULL), NOW())
ON DUPLICATE KEY UPDATE
  home_line = VALUES(home_line),
  visitor_line = VALUES(visitor_line),
  home_spread = VALUES(home_spread),
  visitor_spread = VALUES(visitor_spread),
  over_under = VALUES(over_under),
  last_synced = VALUES(last_synced);')
  or die_on_error("Unable to prepare 'insert' statement: " . $DBI::errstr);

# Loop through the sports and, if relevant, import
foreach my $sport_ref (sort keys %{$config{'sports'}}) {
  my %sport = %{$config{'sports'}{$sport_ref}};
  my $sport_uc = uc $sport_ref;
  log_msg("Attempting to sync $sport_uc odds");

  # Build our query to determine if the sport is relevant
  my $sql = "SELECT SCHED.season, SCHED.game_type, SCHED.game_id,
       SCHED.home, CONCAT(TEAM_HOME.city, ' ', TEAM_HOME.franchise) AS home_name,
       SCHED.visitor, CONCAT(TEAM_VISITOR.city, ' ', TEAM_VISITOR.franchise) AS visitor_name
FROM SPORTS_${sport_uc}_SCHEDULE AS SCHED
LEFT JOIN SPORTS_${sport_uc}_TEAMS AS TEAM_HOME
  ON (TEAM_HOME.team_id = SCHED.home)
LEFT JOIN SPORTS_${sport_uc}_TEAMS AS TEAM_VISITOR
  ON (TEAM_VISITOR.team_id = SCHED.visitor)
WHERE SCHED.game_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL ? - 1 DAY)
AND   CONCAT(SCHED.game_date, ' ', SCHED.game_time) >= CONVERT_TZ(NOW(), 'SYSTEM', ?)
ORDER BY SCHED.game_date, SCHED.game_time, SCHED.game_id;";
  my @db_games = @{ $dbh->selectall_arrayref($sql, { Slice => {} }, ( $sport{'lookahead'}, $sport{'db_timezone'} )) };

  # Skip if no games were found
  if (!@db_games) {
    log_msg('  Skipping, no upcoming games found in the database');
    next;
  }

  # Map the database games into a shorthand accessor hash
  my $index = 0;
  my %db_games_map = ( );
  foreach my $db_game (@db_games) {
    my $matchup = decode_entities($$db_game{'visitor_name'}) . '-@-' . decode_entities($$db_game{'home_name'});
    $db_games_map{$matchup} = $index++;
  }

  # Determine the date range of our search... in UTC
  $sql = "SELECT CAST(MIN(CONVERT_TZ(CONCAT(game_date, ' 00:00:00'), ?, 'UTC')) AS DATETIME) AS utc_min,
       CAST(MAX(DATE_ADD(CONVERT_TZ(CONCAT(game_date, ' 23:59:59'), ?, 'UTC'), INTERVAL 1 SECOND)) AS DATETIME) AS utc_max
FROM SPORTS_${sport_uc}_SCHEDULE
WHERE game_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL ? - 1 DAY);";
  my ($db_game_dates) = @{ $dbh->selectall_arrayref($sql, { Slice => {} }, ( $sport{'db_timezone'}, $sport{'db_timezone'}, $sport{'lookahead'} )) };
  my $date_from = $$db_game_dates{'utc_min'} . 'Z'; $date_from =~ s/ /T/;
  my $date_to = $$db_game_dates{'utc_max'} . 'Z'; $date_to =~ s/ /T/;

  # Form the URL to call on the API
  my $uri = $config{'api'}{'uri'};
  $uri =~ s/{sport}/$sport{'slug'}/g;
  $uri =~ s/{regions}/$sport{'regions'}/g;
  $uri =~ s/{markets}/$sport{'markets'}/g;
  $uri =~ s/{commenceTimeFrom}/${date_from}/g;
  $uri =~ s/{commenceTimeTo}/${date_to}/g;
  my $uri_disp = $uri; # A version to log without the API key in it
  log_msg("  Calling $uri_disp");
  $uri =~ s/{apiKey}/$config{'api'}{'key'}/g;

  # Make the request
  my $ret = $browser->get($uri);
  my $request_txt = $ret->decoded_content; chomp($request_txt);
  if ($ret->code != 200) {
    log_msg("    HTTP Code " . $ret->code . " returned, skipping");
    log_msg("    Response: $request_txt");
    next;
  }
  log_msg("    Quota Cost: " . $ret->header('x-requests-last')
    . "; Used: " . $ret->header('x-requests-used')
    . "; Remaining: " . $ret->header('x-requests-remaining'));
  if (!defined($request_txt) || $request_txt eq '') {
    log_msg("    Empty response received, skipping");
    next;
  }

  # Convert text to a JSON object
  my $odds;
  try {
    $odds = decode_json($request_txt);
  } catch {
    log_msg('    Unable to decode JSON, skipping');
    next;
  };

  # Loop through the games and match to one of ours
  foreach my $game (@$odds) {
    log_msg("  Parsing Game: $$game{'away_team'} \@ $$game{'home_team'}");

    # Identify which database game this relates to
    my $db_games_index = $db_games_map{"$$game{'away_team'}-\@-$$game{'home_team'}"};
    if (!defined($db_games_index)) {
      log_msg('    Unable to map to a database game, skipping');
      next;
    }
    my %db_game = %{$db_games[$db_games_index]};
    my $game_ref = "$db_game{'season'}-$db_game{'game_type'}-$db_game{'game_id'}";
    log_msg("    Mapped to: $game_ref");

    # Loop through the various available odds
    my %raw = (
      'home_line' => [],
      'visitor_line' => [],
      'spread' => [],
      'over_under' => [],
    );
    foreach my $bookie (@{$$game{'bookmakers'}}) {
      foreach my $line (@{$$bookie{'markets'}}) {
        if ($$line{'key'} eq 'h2h') {
          # Moneyline
          my $home_key = ($$line{'outcomes'}[0]{'name'} eq $db_game{'home_name'} ? 0 : 1);
          push @{$raw{'home_line'}}, $$line{'outcomes'}[$home_key]{'price'};
          push @{$raw{'visitor_line'}}, $$line{'outcomes'}[1 - $home_key]{'price'};

        } elsif ($$line{'key'} eq 'spreads') {
          # Point spread
          my $home_key = ($$line{'outcomes'}[0]{'name'} eq $db_game{'home_name'} ? 0 : 1);
          push @{$raw{'spread'}}, $$line{'outcomes'}[$home_key]{'point'};

        } elsif ($$line{'key'} eq 'totals') {
          # Over/Under
          push @{$raw{'over_under'}}, $$line{'outcomes'}[0]{'point'};
        }
      }
    }
    my $skip_insert = 0; # Assume we can do the insert, unless told otherwise

    # Moneylines
    my $home_line; my $visitor_line;
    log_msg('    Moneylines:');
    if (@{$raw{'home_line'}}) {
      $home_line = pseudo_median(@{$raw{'home_line'}});
      if ($home_line) {
        my $home_line_us = sprintf('%d', $home_line >= 2 ? ($home_line - 1) * 100 : -100 / ($home_line - 1));
        log_msg("      $db_game{'home'}: $home_line ($home_line_us)");
      } else {
        log_msg("      WARNING - No home moneylines was calculated in $sport_ref game $game_ref for inputs: " . join(',', @{$raw{'home_line'}}));
        $skip_insert = 1;
      }
    } else {
      log_msg("      WARNING - No home moneylines were returned in $sport_ref game $game_ref");
      $skip_insert = 1;
    }
    if (@{$raw{'visitor_line'}}) {
      $visitor_line = pseudo_median(@{$raw{'visitor_line'}});
      if ($visitor_line) {
        my $visitor_line_us = sprintf('%d', $visitor_line >= 2 ? ($visitor_line - 1) * 100 : -100 / ($visitor_line - 1));
        log_msg("      $db_game{'visitor'}: $visitor_line ($visitor_line_us)");
      } else {
        log_msg("      WARNING - No visitor moneylines was calculated in $sport_ref game $game_ref for inputs: " . join(',', @{$raw{'visitor_line'}}));
        $skip_insert = 1;
      }
    } else {
      log_msg("      WARNING - No visitor moneylines were returned in $sport_ref game $game_ref");
      $skip_insert = 1;
    }
    # Point spread (Not always expected to be returned)
    my $home_spread; my $visitor_spread;
    if ($sport{'markets'} =~ /\bspreads\b/) {
      log_msg('    Point Spread:');
      if (@{$raw{'spread'}}) {
        $home_spread = pseudo_median(@{$raw{'spread'}});
        log_msg("      $db_game{'home'}: $home_spread");
        $visitor_spread = 0 - $home_spread;
        log_msg("      $db_game{'visitor'}: $visitor_spread");
      } else {
        log_msg("      WARNING - Point spreads expected, but none were returned in $sport_ref game $game_ref");
        $skip_insert = 1;
      }
    }
    # Over/Under
    my $over_under;
    if (@{$raw{'over_under'}}) {
      $over_under = pseudo_median(@{$raw{'over_under'}});
      log_msg("    Over/Under: $over_under");
    } else {
      log_msg("    WARNING - No over/unders were returned in $sport_ref game $game_ref");
      $skip_insert = 1;
    }

    # If we can, perform the insert
    if (!$skip_insert) {
      $ins_sth->execute(
        $sport_ref, $db_game{'season'}, $db_game{'game_type'}, $db_game{'game_id'},
        $home_line, $visitor_line, $home_spread, $visitor_spread,
        int(defined($over_under)), $over_under
      ) or log_msg("  ERROR - Unable to execute 'insert' statement in $sport_ref game $game_ref: " . $DBI::errstr);
    } else {
      # We do not need to repeat this as a warning or error, as that would be captured above
      log_msg("  Import skipped due to missing details in $sport_ref game $game_ref");
    }
  }
}

# Disconnect from the database
$ins_sth->finish if defined($ins_sth);
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log();

##
## Sub-routines used above
##
# Ensures a value from the list is returned, not the statistical model which can be halfway between two values in even sized lists
sub pseudo_median {
  my @arr = @_;
  my @sorted = sort {$a <=> $b} @arr;
  return $sorted[floor($#sorted / 2) - 1];
}
