#!/usr/bin/perl -w
# Perl config file

our %config;
require $base_dir . '/../config.pl';

# Scheduled run time is 8am Europe/London
$config{'scheduled'} = {
  'timezone' => 'Europe/London',
  'hour' => 8,
};

# API info
$config{'api'} = {
  'key' => '',
  'uri' => 'https://api.the-odds-api.com/v4/sports/{sport}/odds?apiKey={apiKey}&regions={regions}&markets={markets}&oddsFormat=decimal&commenceTimeFrom={commenceTimeFrom}&commenceTimeTo={commenceTimeTo}',
};
$config{'api'}{'key'} = do { local(@ARGV, $/) = "$config{'passwd_dir'}/api/odds/the-odds-api"; <> }; chomp($config{'api'}{'key'});
$config{'api'}{'key'} = decode_base64($config{'api'}{'key'});

# Sports to sync and import
$config{'sports'} = {
  'nfl' => {
    'slug' => 'americanfootball_nfl',
    'regions' => 'us',
    'markets' => 'h2h,spreads,totals',
    'lookahead' => 7,
    'db_timezone' => 'America/New_York',
  },
  'mlb' => {
    'slug' => 'baseball_mlb',
    'regions' => 'us',
    'markets' => 'h2h,spreads,totals',
    'lookahead' => 1,
    'db_timezone' => 'America/New_York',
  },
  'nhl' => {
    'slug' => 'icehockey_nhl',
    'regions' => 'us',
    'markets' => 'h2h,spreads,totals',
    'lookahead' => 1,
    'db_timezone' => 'America/New_York',
  },
};

# Database setup customisations
$config{'db'}{'name'} = $config{'db'}{'user'} = 'debearco_sports';

# Log message
$config{'log_dir'} = $base_dir . '/../../logs/common/sports_odds';
$config{'log_file'} = {
  'fmt' => '%Y-%m-%d',
  'ext' => 'log.gz' };
$config{'log'} = '';

##
## Sub-routines
##
#
# Check if an argument we have been provided exists in the $opt object
#
sub is_opt {
  our %opt;
  my ($arg_ref) = @_;
  my ($arg) = ($$arg_ref =~ m/^--(.+)$/);

  # This is a valid $opt argument, unless...
  return 0
    if !defined($arg)                # It does not begin with the dashes
       || !grep(/^$arg$/, keys %opt) # It does not exist in $opt
       || $opt{$arg};                # It has already been set

  # It's a valid argument, so update $arg_ref (so when we use it later it doesn't have the dashed start and return valid
  $$arg_ref = $arg;
  return 1;
}

#
# Establish where to save the log
#
sub setup_logging {
  my $log_date = strftime($config{'log_file'}{'fmt'}, localtime);
  $config{'log_file'} = $config{'log_dir'}. '/' . $log_date . '.' . $config{'log_file'}{'ext'};
  $config{'log'} = '==========' . "\n";
}

#
# Record a log message with a timestamp
#
sub log_msg {
  my ($msg) = @_;
  $config{'log'} .= strftime('%Y-%m-%d %H:%M:%S', localtime) . ' - ' . $msg . "\n";
}

#
# Save the log to file
#
sub save_log {
  our %opt;
  # If we're displaying to STDOUT instead, do so
  if ($opt{'no-save-log'}) {
    print $config{'log'};
    return;;
  }

  # The saving...
  output_file($config{'log_file'}, $config{'log'});
}

#
# Die (and log reason...) if an error occurred
#
sub die_on_error {
  my ($err) = @_;
  log_msg('ERROR - ' . $err);
  push @errors, 'E - ' . $err;
  save_log();
  exit;
}

# Return true to pacify the compiler
1;
