#!/usr/bin/perl -w
# Archive log files built up over time

use strict;
use File::Basename;
use Data::Dumper;
use POSIX qw/strftime/;

# Mask the filename as we're on a shared server
$0 = basename $0;

# Identify the current month, to avoid archiving its logs
my $cur_month = strftime('%Y-%m', localtime);

# Check if debug mode passed
my $debug = (@ARGV > 0 && $ARGV[0] eq '--debug');

my @all_dirs = ({ 'dir' => '~/debear/logs', 'ext' => 'log', 'levels' => 2 }, # Standard logfiles
                { 'dir' => '~/debear/logs', 'ext' => 'json', 'levels' => 2 }, # Browser reports (as JSON bundles)
                { 'dir' => '~/debear/backups/archives', 'ext' => 'sql', 'levels' => 1 });

# Loop through the directories we're pruning
foreach my $base (@all_dirs) {
  my @outer = glob($$base{'dir'} . '/*');
  foreach my $outer (@outer) {
    next if ! -d $outer;
    print "**\n** $outer:\n**\n" if $debug;

    # Then its sub-folders
    my @dirs;
    if ($$base{'levels'} == 2) {
      @dirs = glob($outer . '/*');
    } else {
      @dirs = ( $outer );
    }

    # Find any logs in each dir from a previous month
    foreach my $dir (@dirs) {
      next if ! -d $dir;

      my %logs = ( );
      chdir "$dir";
      foreach my $file (glob("*.$$base{'ext'}*")) {
        # Skip files that aren't dated for archiving
        next if $file !~ m/^(\d{4}-\d{2})/si;
        # Skip files for the current month
        my ($month) = ($file =~ m/^(\d{4}-\d{2})/si);
        next if $month eq $cur_month;

        # If the log has not been gzipped, do so now
        if ($file !~ /\.gz$/) {
          run_cmd("gzip $file");
          $file .= '.gz';
        }

        # Store for our next loop when we'll tar the files by month
        $logs{$month} = ( ) if !defined($logs{$month});
        push @{$logs{$month}}, $file;
      }

      # Archive the individual files, if there are any
      foreach my $month (sort keys %logs) {
        # Are we creating or updating this tar file?
        my $tar_exists = (-e "$month.tar");
        my $mode = ($tar_exists ? 'u' : 'c');
        print "**** $dir (Mode: $mode)\n" if $debug;
        # Run
        run_cmd("tar -${mode}f $month.tar " . join(' ', @{$logs{$month}}));
        print Dumper($logs{$month}) if $debug;
        unlink @{$logs{$month}} if !$debug;
      }
    }
    print "\n" if $debug;
  }
}

#
# Run a command outside of Perl
#
sub run_cmd {
  my ($cmd) = @_;

  print "$cmd\n" if $debug;
  `$cmd` if !$debug;
}
