#!/usr/bin/perl -w
# Rotate log to our archivable format

use strict;
use File::Basename;
use Data::Dumper;
use POSIX qw/strftime/;

# Mask the filename as we're on a shared server
$0 = basename $0;

# Identify the previous day, so we can label it correctly
my @t = localtime; $t[3]--;
my $log_date = strftime('%Y-%m-%d', @t);

# Check if debug mode passed
my $debug = (@ARGV > 0 && $ARGV[0] eq '--debug');

# What are we processing?
my @all_logs = (
  '~/debear/logs/skeleton/laravel/laravel.log', # Laravel's log
);

foreach my $log (@all_logs) {
  print "- $log: " if $debug;
  if (! -e $log) {
    print "[ Not Found ]\n" if $debug;
    next;
  }
  print "\n" if $debug;

  # Step 0: Prepare the action info
  my ($log_file, $log_dir, $log_ext) = fileparse($log, qr/\.[^.]*/);
  my $log_rotated = "$log_dir$log_date$log_ext";

  # Step 1: Rename and compress
  run_cmd("mv $log $log_rotated");
  run_cmd("gzip $log_rotated");

  # Step 2: Re-create the original file
  run_cmd("touch $log");
}

#
# Run a command outside of Perl
#
sub run_cmd {
  my ($cmd) = @_;

  print "  - $cmd\n" if $debug;
  `$cmd` if !$debug;
}
