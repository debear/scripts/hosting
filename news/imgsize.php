<?php
    try {
        // Ensure we have an image argument.
        if (!isset($argv[1]) || !($img = trim($argv[1])) || !file_exists($img)) {
            throw new Exception('Invalid filename argument');
        }

        // Attempt to get the dimensions of the (assumed) image.
        $info = getimagesize($img);
        if (!is_array($info)) {
            throw new Exception('Invalid image size return type');
        }

        // Render the dimension result.
        print "{$info[0]}x{$info[1]}";
    } catch (Exception $e) {
        // Our error handling.
        print '';
        exit(1);
    }
