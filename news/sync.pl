#!/usr/bin/perl -w
# Download and import news from RSS feeds

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $XML::Simple::PREFERRED_PARSER = 'XML::Parser';
  }
}

use strict;
use utf8;
use DBI;
use LWP::UserAgent;
use XML::Parser;
use XML::Simple;
use Encode;
use Date::Manip;
use HTML::Entities;
use Digest::MD5 qw(md5_hex);
use Data::Dumper;

# Mask the filename as we're on a shared server
our $base_dir;
our $script_raw = $0;
$0 = basename $0;

# Get base config
our %config;
require $base_dir . '/config.pl';

# Logging info
setup_logging();

# Flag to keep track of any database problems
my $warn_flag;
my @errors = ( );

# Validate arguments
our %opt = (
  'debug'       => 0, # Only get the _list_ of news items, do not store in the database
  'extended'    => 0, # Log individual news items, not just numbers
  'no-save-log' => 0, # Display the log on STDOUT, not to file
);
foreach my $arg (@ARGV) {
  # Is it an option we're looking for?
  if (is_opt(\$arg)) {
    log_msg('* Running in \'' . $arg . '\' mode *');
    $opt{$arg} = 1;

  # Not expecting anything, so log we don't know what to do with the argument, and ignore it
  } else {
    log_msg('* Unknown argument \'' . $arg . '\'');
  }
}

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
$dbh->do('SET time_zone = "Europe/London";')
  or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);
my $sth;

# Get the list of feeds to import
my $sql = 'SELECT app, source_id, rss_feed
FROM NEWS_SOURCES
WHERE DATE_ADD(last_synced, INTERVAL IFNULL(ttl, 15) MINUTE) <= NOW()
AND   active = 1
ORDER BY app, source_id;';
my @sources = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };

# If no sources found, flag and quit
if (!scalar(@sources)) {
  log_msg('No news sources require re-syncing')
    if $opt{'debug'};

# If we have news to get, get it
} else {
  # Prepare the database statements
  my $source_sth = $dbh->prepare('UPDATE NEWS_SOURCES
SET title = ?,
    homepage = ?,
    description = ?,
    rss_copyright = ?,
    ttl = ?,
    logo = ?,
    logo_title = ?,
    logo_link = ?,
    logo_width = ?,
    logo_height = ?,
    last_updated = ?,
    last_synced = NOW()
WHERE app = ?
AND   source_id = ?;')
    or die_on_error('Unable to prepare $source_sth: ' . $DBI::errstr)
    if !$opt{'debug'};
  my $news_sth = $dbh->prepare('INSERT INTO NEWS_ARTICLES (app, news_id, source_id, title, description, link_domain, link_location, link, published, thumbnail, thumbnail_domain, thumbnail_width, thumbnail_height)
  VALUES (?, ?, ?, ?, ?, IF(? = 0, NULL, ?), IF(? = 0, NULL, ?), ?, ?, IF(? = -1, NULL, ?), IF(? = -1, NULL, ?), IF(? = -1, NULL, ?), IF(? = -1, NULL, ?))
ON DUPLICATE KEY UPDATE title = VALUES(title),
                        description = VALUES(description),
                        link_domain = VALUES(link_domain),
                        link_location = VALUES(link_location),
                        link = VALUES(link),
                        published = VALUES(published),
                        thumbnail = VALUES(thumbnail),
                        thumbnail_domain = VALUES(thumbnail_domain),
                        thumbnail_width = VALUES(thumbnail_width),
                        thumbnail_height = VALUES(thumbnail_height);')
    or die_on_error('Unable to prepare $news_sth: ' . $DBI::errstr)
    if !$opt{'debug'};
  my $link_img_sth = $dbh->prepare('SELECT thumbnail, thumbnail_domain, thumbnail_width, thumbnail_height from NEWS_ARTICLES WHERE link = ? ORDER BY thumbnail IS NULL LIMIT 1;')
    or die_on_error('Unable to prepare $link_img_sth: ' . $DBI::errstr);
  my $img_size_sth = $dbh->prepare('SELECT thumbnail_width, thumbnail_height from NEWS_ARTICLES WHERE thumbnail = ? ORDER BY thumbnail_width IS NULL LIMIT 1;')
    or die_on_error('Unable to prepare $img_size_sth: ' . $DBI::errstr);

  # Create our browser object
  my $browser = new LWP::UserAgent(
    ssl_opts => { verify_hostname => 0 }, # Disable SSL cert checks (potentially worrying?)
  );
  $browser->agent($config{'user_agent'});

  # Now loop through the news sources
  foreach my $source (@sources) {
    $warn_flag = 0;
    log_msg('Source \'' . $$source{'app'} . '\', ID \'' . $$source{'source_id'} . '\', from ' . $$source{'rss_feed'});

    # First, download the RSS
    my $req = $browser->get($$source{'rss_feed'});
    if (!$req->is_success) {
      log_msg(' -> No feed found, skipping. (Returned Status: ' . $req->status_line . ')');
      next;
    }
    my $rss = $req->decoded_content;

    # "Fix" XML issues
    fix_rss(\$rss);
    if (!defined($rss) || !$rss) {
      log_msg(' -> No feed found after fixing, skipping.');
      next;
    }

    # Convert the XML into an object we can parse
    my $p = new XML::Simple;
    my $full_feed = eval { $p->XMLin($rss) };

    # Any errors?
    if ($@) {
      log_msg(' -> Invalid feed, skipping.');
      my $err_info = substr($@, 0, 300); chomp($err_info);
      log_msg(' -> \'' . $err_info . '\'');
      # Write to disk for future examination
      my $xml_file = $config{'log_dir'} . '/failed_' . $$source{'app'} . '-' . $$source{'source_id'} . '.xml.gz';
      output_file($xml_file, $rss);
      next;
    }

    # If it's an Atom feed, convert it to RSS
    my $feed = $$full_feed{'channel'};
    $feed = convert_atom($rss)
      if !defined($feed);
    # Ensure the item list has been converted to an array
    $$feed{'item'} = [ ]
      if !defined($$feed{'item'}) || (ref($$feed{'item'}) ne 'ARRAY');

    # When does the source say it was last updated (aka "built" or "published")
    if (defined($$feed{'lastBuildDate'})) {
      $$feed{'last_updated'} = convert_datetime($$feed{'lastBuildDate'});
    } elsif (defined($$feed{'pubDate'})) {
      $$feed{'last_updated'} = convert_datetime($$feed{'pubDate'});
    }
    # Fallback?
    $$feed{'last_updated'} = strftime('%Y-%m-%d %H:%M:%S', localtime)
      if !defined($$feed{'last_updated'});
    log_msg(' -> \'' . $$feed{'title'} . '\', Last Updated ' . $$feed{'last_updated'});

    # Update the main info
    log_msg(' -> ' . (scalar @{$$feed{'item'}}) . ' articles found.');
    $source_sth->execute(
      encode_entities($$feed{'title'}),
      $$feed{'link'},
      !defined($$feed{'description'}) ? '' : encode_entities($$feed{'description'}),
      !defined($$feed{'copyright'}) ? undef : encode_entities($$feed{'copyright'}),
      !defined($$feed{'ttl'}) ? undef : $$feed{'ttl'},
      $$feed{'image'}{'url'},
      !defined($$feed{'image'}{'title'}) ? undef : encode_entities($$feed{'image'}{'title'}),
      $$feed{'image'}{'link'},
      $$feed{'image'}{'width'}, $$feed{'image'}{'height'},
      $$feed{'last_updated'},
      $$source{'app'}, $$source{'source_id'}
    ) or warn_on_error('Unable to execute $source_sth for ' . $$source{'app'} . ' feed "' . $$source{'source_id'} . '": ' . $DBI::errstr)
      if !$opt{'debug'};

    # Skip this feed if the above query returned an error
    next if $warn_flag;

    # Then loop through the individual articles
    my $last_item_published = convert_datetime('now');
    foreach my $item (@{$$feed{'item'}}) {
      # Check for articles that void of details
      next if ((ref($$item{'pubDate'}) eq 'HASH') && (ref($$item{'title'}) eq 'HASH')) || (ref($$item{'link'}) eq 'HASH');

      # Parse the link
      my ($link_domain, $link_location) = ($$item{'link'} =~ m/^(https?:\/\/[^\/]+)(\/.+)$/i);
      my $link_defined = (defined($link_domain) && $link_domain ne '' ? 1 : 0);
      if ($link_defined) {
        $link_domain =~ s/(\/\/)www\./$1/;
      } else {
        $link_domain = '';
      }
      $link_location = '' if !$link_defined;
      $link_location =~ s/\?.+$//
        if $link_location =~ /\?/;

      # Get the published date in the right format, and log
      $$item{'published'} = convert_datetime($$item{'pubDate'})
        if defined($$item{'pubDate'});
      # If nothing, use the last date we had...
      $$item{'published'} = $last_item_published
        if !defined($$item{'published'});
      $last_item_published = $$item{'published'};
      log_msg('    -> \'' . $$item{'title'} . '\', Last Updated ' . $$item{'published'})
        if $opt{'extended'};

      # We'll use the larger image (if there is one), so find its spot in the list
      my $img_index = -1;
      $$item{'media:thumbnail'} = [ $$item{'media:thumbnail'} ] if ref($$item{'media:thumbnail'}) eq 'HASH';
      if (ref($$item{'media:thumbnail'}) eq 'ARRAY') {
        my @img = @{$$item{'media:thumbnail'}};
        my $last = -1;
        for (my $i = 0; $i < @{$$item{'media:thumbnail'}}; $i++) {
          if ($last < $$item{'media:thumbnail'}[$i]{'width'}) {
            $img_index = $i;
            $last = $$item{'media:thumbnail'}[$i]{'width'};
          }
        }
        log_msg('      -> Image URL from media:thumbnail')
          if $opt{'extended'};
      # Though it could be defined in an enclosure
      } elsif (defined($$item{'enclosure'})) {
        if (defined($$item{'enclosure'}{'type'}) && substr($$item{'enclosure'}{'type'}, 0, 6) == 'image/') {
          $img_index = 0;
          $$item{'media:thumbnail'} = [{
            'url' => ref($$item{'enclosure'}{'url'}) eq 'ARRAY' ? $$item{'enclosure'}{'url'}[0] : $$item{'enclosure'}{'url'},
            'width' => undef,
            'height' => undef,
          }];
        }
        log_msg('      -> Image URL from enclosure')
          if $opt{'extended'};
      # Or a more generic image tag
      } elsif (defined($$item{'image'})) {
        $img_index = 0;
        my $item_img;
        if (ref($$item{'image'}) eq 'HASH') {
          $item_img = (ref($$item{'image'}{'url'}) eq 'ARRAY' ? $$item{'image'}{'url'}[0] : $$item{'image'}{'url'});
        } else {
          $item_img = $$item{'image'};
        }
        $$item{'media:thumbnail'} = [{
          'url' => $item_img,
          'width' => undef,
          'height' => undef,
        }];
        log_msg('      -> Image URL from image attribute')
          if $opt{'extended'};
      # And if neither of these, fallback to searching for an OG image
      } elsif (defined($$item{'link'})) {
        # Have we already got an image for this link?
        $link_img_sth->execute($$item{'link'});
        my $existing = $link_img_sth->fetchrow_hashref;
        if (defined($$existing{'thumbnail'})) {
          # Yes, a match
          $img_index = 0;
          $$item{'media:thumbnail'} = [{
            'url' => $$existing{'thumbnail'},
            'width' => $$existing{'thumbnail_width'},
            'height' => $$existing{'thumbnail_height'},
          }];
          log_msg('      -> Image URL from prior match')
            if $opt{'extended'};
        } else {
          # First time we've come across it, so attempt to read it og:image
          my $article = `wget -U"$config{'user_agent'}" -qO- "$$item{'link'}"`;
          $img_index = 0;
          # Attempt 1: attributes enclosed with "
          my ($og_img) = ($article =~ m/<meta property="og:image" content="([^"]+)"/gsi);
          # Attempt 2: attributes enclosed with ''
          ($og_img) = ($article =~ m/<meta property='og:image' content='([^']+)'/gsi)
            if !defined($og_img) || $og_img =~ /^\s*$/;
          $$item{'media:thumbnail'}[$img_index]{'url'} = $og_img;
          # How about an og:image:width / og:image:height?
          ($$item{'media:thumbnail'}[$img_index]{'width'}) = ($article =~ m/<meta property="og:image:width" content="(\d+)"/gsi);
          ($$item{'media:thumbnail'}[$img_index]{'height'}) = ($article =~ m/<meta property="og:image:height" content="(\d+)"/gsi);
          log_msg('      -> Image URL from og:image (' . (defined($og_img) ? $og_img : 'undef') . '; Resp Len: ' . length($article) . ')')
            if $opt{'extended'};
        }
      }

      # Are we mising thumbnail dimensions?
      if (defined($$item{'media:thumbnail'}[$img_index]{'url'})
        && (!defined($$item{'media:thumbnail'}[$img_index]{'width'})
          || !defined($$item{'media:thumbnail'}[$img_index]{'height'}))) {
        # Have we already checked this image?
        $img_size_sth->execute($$item{'media:thumbnail'}[$img_index]{'url'});
        my $existing = $img_size_sth->fetchrow_hashref;
        if (defined($$existing{'thumbnail_width'})) {
          $$item{'media:thumbnail'}[$img_index]{'width'} = $$existing{'thumbnail_width'};
          $$item{'media:thumbnail'}[$img_index]{'height'} = $$existing{'thumbnail_height'};
          log_msg('      -> Image dimensions from prior match')
            if $opt{'extended'};
        } else {
          # Download and store locally once to test
          my $img_tmp = $config{'tmp_dir'} . substr(md5_hex($$item{'media:thumbnail'}[$img_index]{'url'}), 0, 16);
          `wget -U"$config{'user_agent'}" -qO$img_tmp "$$item{'media:thumbnail'}[$img_index]{'url'}"`
            if ! -e $img_tmp;
          my $img_dim = `php $base_dir/imgsize.php $img_tmp`;
          my $img_dim_ret = ($img_dim =~ m/^\d+x\d+$/);
          ($$item{'media:thumbnail'}[$img_index]{'width'}, $$item{'media:thumbnail'}[$img_index]{'height'}) = ($img_dim =~ m/^(\d+)x(\d+)$/gsi)
            if $img_dim_ret;
          unlink($img_tmp);
          log_msg('      -> Image dimensions from download / imgsize.php')
            if $opt{'extended'} && $img_dim_ret;
        }
      }

      # Parse the thumbnail domain?
      if ($img_index != -1) {
        my ($thumbnail_domain, $thumbnail_location) = ($$item{'media:thumbnail'}[$img_index]{'url'} =~ m/^(https?:\/\/[^\/]+)(\/.+)$/i);
        my $thumbnail_defined = (defined($thumbnail_domain) && $thumbnail_domain ne '' ? 1 : 0);
        if ($thumbnail_defined) {
          $thumbnail_domain =~ s/(\/\/)www\./$1/;
        } else {
          $thumbnail_domain = '';
        }
        $$item{'media:thumbnail'}[$img_index]{'url_domain'} = $thumbnail_domain;
      }

      # Now add
      my @args = (
        $$source{'app'}, undef, $$source{'source_id'},
        encode_entities($$item{'title'}),
        encode_entities($$item{'description'}),
        $link_defined, $link_domain,
        $link_defined, $link_location,
        $$item{'link'}, $$item{'published'},
        $img_index, $img_index == -1 ? '' : $$item{'media:thumbnail'}[$img_index]{'url'},
        $img_index, $img_index == -1 ? '' : $$item{'media:thumbnail'}[$img_index]{'url_domain'},
        $img_index, $img_index == -1 ? '' : $$item{'media:thumbnail'}[$img_index]{'width'},
        $img_index, $img_index == -1 ? '' : $$item{'media:thumbnail'}[$img_index]{'height'},
      );
      $news_sth->execute(@args)
        or warn_on_error('Unable to execute $news_sth for ' . $$source{'app'} . '//' . $$source{'source_id'} . ' article "' . encode_entities($$item{'title'}) . '": ' . $DBI::errstr)
        if !$opt{'debug'};
      log_msg('      -> ' . Dumper(\@args))
        if $opt{'debug'} && $opt{'extended'};
      log_msg("      -> Storing with index: $img_index")
        if !$opt{'debug'} && $opt{'extended'};
      log_msg('      -> ' . Dumper($$item{'media:thumbnail'}[$img_index]))
        if !$opt{'debug'} && $opt{'extended'};
    }
    log_msg(' -> Processing source \'' . $$source{'app'} . '\', ID \'' . $$source{'source_id'} . '\' complete');
  }
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log()
  if scalar(@sources) || $opt{'debug'};

##
## Sub-routines used above
##
#
# Check if an argument we have been provided exists in the $opt object
#
sub is_opt {
  our %opt;
  my ($arg_ref) = @_;
  my ($arg) = ($$arg_ref =~ m/^--(.+)$/);

  # This is a valid $opt argument, unless...
  return 0
    if !defined($arg)                # It does not begin with the dashes
       || !grep(/^$arg$/, keys %opt) # It does not exist in $opt
       || $opt{$arg};                # It has already been set

  # It's a valid argument, so update $arg_ref (so when we use it later it doesn't have the dashed start and return valid
  $$arg_ref = $arg;
  return 1;
}

#
# Convert the feed from Atom to RSS
#
sub convert_atom {
  my ($rss) = @_;
  my %feed = ( );

  # Convert the input into an XML object
  my $p = new XML::Simple;
  $rss = $p->XMLin($rss);

  # Feed info
  $feed{'title'} = defined($$rss{'title'}) ? $$rss{'title'} : undef;
  $feed{'link'} = defined($$rss{'link'}) && ref($$rss{'link'}) ne 'ARRAY' ? $$rss{'link'} : top_level_url($$rss{'id'});
  $feed{'description'} = defined($$rss{'subtitle'}) ? $$rss{'subtitle'} : undef;
  $feed{'copyright'} = defined($$rss{'rights'}) ? $$rss{'rights'} : undef;
  $feed{'ttl'} = defined($$rss{'ttl'}) ? $$rss{'ttl'} : undef;
  $feed{'lastBuildDate'} = defined($$rss{'updated'}) ? $$rss{'updated'} : undef;

  $feed{'image'} = { 'url' => undef,
                     'title' => undef,
                     'link' => undef,
                     'width' => undef,
                     'height' => undef };

  # Individual articles
  $feed{'item'} = ( );
  # First though, get the ordered list by timestamp (our hash is in a random order)
  my @ids = sort
              { convert_datetime($$rss{'entry'}{$b}{'published'}, '%s') <=> convert_datetime($$rss{'entry'}{$a}{'published'}, '%s') }
              keys %{$$rss{'entry'}};
  foreach my $id (@ids) {
    my %entry = (
      'pubDate' => $$rss{'entry'}{$id}{'published'},
      'title' => $$rss{'entry'}{$id}{'title'},
      'description' => $$rss{'entry'}{$id}{'summary'}{'content'},
      'link' => $id
    );

    push @{$feed{'item'}}, \%entry;
  }

  return \%feed;
}

#
# Domain root for a given URL
#
sub top_level_url {
  my ($url) = @_;
  my ($tl_url) = ($url =~ m/^(https?:\/\/[^\/]+\/)/si);

  return defined($tl_url) ? $tl_url : $url;
}

#
# Resolve issues with the RSS feed
#
sub fix_rss {
  my ($rss) = @_;

  # Ensure a consistent encoding
  $$rss = Encode::encode('UTF-8', $$rss);

  # Add missing CDATA tags
#  foreach my $tag ('title', 'description') {
#    $$rss =~ s/(<$tag>)(?:<\!\[CDATA\[){0}(.*?)(?:]]){0}(<\/$tag>)/$1<![CDATA[$2]]>$3/g
#      if $$rss !~ /<$tag>.*?<\!\[CDATA\[.*?<\/$tag>/g;
#  }

  # Fix dodgy char issues
  foreach my $char (
    '1A', # 26 (Base 10) - non-display whitespace
  ) {
    $$rss =~ s/\x$char//g;
  }
}

#
# Convert a time, where there may be issues with the string passed in
#
sub convert_datetime {
  my ($time_str, $time_fmt) = @_;
  $time_str =~ s/^\s+//; $time_str =~ s/\s+$//;
  $time_fmt = '%Y-%m-%d %H:%M:%S' if !defined $time_fmt;

  my $cnv = UnixDate($time_str, $time_fmt);
  return $cnv
    if convert_datetime_valid($cnv);

  # Fix 1: [ECP]DT during winter?
  if ($time_str =~ m/DT$/) {
    $time_str =~ s/DT$/ST/;
    $cnv = UnixDate($time_str, $time_fmt);
    return $cnv
      if convert_datetime_valid($cnv);
  }

  # Fix 2: Missing standard/daytime info?
  #  Only instance is ET so far...?
  if ($time_str =~ m/ET$/) {
    $time_str =~ s/ET$/EDT/;
    $cnv = UnixDate($time_str, $time_fmt);
    return $cnv
      if convert_datetime_valid($cnv);
  }

  return undef;
}

sub convert_datetime_valid {
  my ($cnv) = @_;
  return (defined($cnv) && $cnv ne '');
}
