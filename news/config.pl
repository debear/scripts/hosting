#!/usr/bin/perl -w
# Perl config file

our %config;
require $base_dir . '/../config.pl';

$config{'user_agent'} = 'RSS:Sync/1.0';
$config{'tmp_dir'} = '/tmp/';

# Warnings to not bother emailing (as they occur frequently, and aren't sufficiently actionable)
@{$config{'ignore_warnings'}} = (
  'Lost connection to MySQL server during query',
  'MySQL server has gone away'
);
# Log message
$config{'log_dir'} = $base_dir . '/../../logs/common/news';
$config{'log_file'} = {
  'fmt' => '%Y-%m-%d',
  'ext' => 'log.gz'
};
$config{'log'} = '';

##
## Sub-routines
##
#
# Establish where to save the log
#
sub setup_logging {
  my $log_date = strftime($config{'log_file'}{'fmt'}, localtime);
  $config{'log_file'} = $config{'log_dir'}. '/' . $log_date . '.' . $config{'log_file'}{'ext'};
  $config{'log'} = '==========' . "\n";
}

#
# Die (and log reason...) if an error occurred
#
sub die_on_error {
  my ($err) = @_;
  log_msg('ERROR - ' . $err);
  push @errors, 'E - ' . $err;
  save_log();
  exit;
}

#
# Warn (and log reason...) if an error occurred, allowing is to continue
#
sub warn_on_error {
  my ($err) = @_;
  log_msg('WARNING - ' . $err);
  push @errors, 'W - ' . $err
    if !ignore_warning($err);
  $warn_flag = 1;
}

#
# Check to see if a warning is common enough that we do not want to email it
#
sub ignore_warning {
  my ($err) = @_;

  # Loop through the list and look for matches
  foreach (@{$config{'ignore_warnings'}}) {
    return 1 if $err =~ /$_/si;
  }

  # Not in the list, so not one we are ignoring
  return 0;
#  grep(/$e/, @{$config{'ignore_warnings'}})
}

#
# Record a log message with a timestamp
#
sub log_msg {
  my ($msg) = @_;
  $config{'log'} .= strftime('%Y-%m-%d %H:%M:%S', localtime) . ' - ' . $msg . "\n";
}

#
# Save the log to file
#
sub save_log {
  our %opt;
  # If we're displaying to STDOUT instead, do so
  if ($opt{'no-save-log'} || $opt{'debug'}) {
    print $config{'log'};
    return if $opt{'no-save-log'};
  }

  # The saving...
  output_file($config{'log_file'}, $config{'log'});
}

# Return true to pacify the compiler
1;
