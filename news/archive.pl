#!/usr/bin/perl -w
# Archive old articles from the database

#
# BEGIN: Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;
  use Cwd 'abs_path';

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Mask the filename as we're on a shared server
  our $script_raw = $0;
  our $script = basename($0, '.pl');
  $0 = basename $0;

  # Some path customisation needs to be made if running on the prod server
  our $home_dir;
  our $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = abs_path(dirname(__FILE__));
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $home_dir = $xilo_home;
  } else {
    $home_dir = '/var/www';
  }
}

use strict;
use utf8;
use File::Basename;
use DBI;

# Get base config
our %config; our $base_dir;
require $base_dir . '/config.pl';

# Logging info
setup_logging();
log_msg('*** Archive ***');

# Output info
my $archive_dir = $base_dir . '/archive';

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
$dbh->do('SET time_zone = "Europe/London";')
  or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);
my $sql;

# Prepare our SQL archive statement
my $del_sth = $dbh->prepare('DELETE FROM NEWS_ARTICLES WHERE app = ? AND news_id = ?;');

# Understand the columns in the table (so we can form the archive SQL)
$sql = 'SHOW CREATE TABLE NEWS_ARTICLES;';
my $res = $dbh->selectall_arrayref($sql);
my $create = $$res[0][1]; # $res = ( ( table name, create statement ) )
my ($cols) = ($create =~ m/\([^\n]*\n(.+)PRIMARY KEY/gsi);
my @cols = ($cols =~ m/\`([^\`]+)\`/gsi);

# Get the list of sources to process
$sql = 'SELECT app, source_id, name, IFNULL(max_articles, 50) AS max_articles
FROM NEWS_SOURCES
WHERE active = 1
ORDER BY app, source_id;';
my @sources = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };

# Loop through them, archiving appropriate articles
my %archive = ( );
foreach my $source (@sources) {
  # Get the appropriate articles
  $sql = 'SELECT *
  FROM NEWS_ARTICLES
  WHERE app = ?
  AND   source_id = ?
  ORDER BY app, published DESC
  LIMIT ?, 9999;';
  my @articles = @{ $dbh->selectall_arrayref($sql, { Slice => {} }, ( $$source{'app'}, $$source{'source_id'}, $$source{'max_articles'} )) };

  if (scalar(@articles) > 0) {
    $archive{$$source{'app'}} = { }
      if !defined($archive{$$source{'app'}});
    log_msg('"' . $$source{'app'} . '" // "' . $$source{'source_id'} . '" // "' . $$source{'name'} . '" -> ' . scalar(@articles) . ' article(s) found.')
  }

  # Convert to an SQL statement, and add to our query
  foreach my $article (reverse @articles) {
    my $month = substr($$article{'published'}, 0, 7);
    $archive{$$source{'app'}}{$month} = { 'ids' => [ ], 'sql' => [ ] }
      if !defined($archive{$$source{'app'}}{$month});

    push @{$archive{$$source{'app'}}{$month}{'ids'}}, $$article{'news_id'};
    push @{$archive{$$source{'app'}}{$month}{'sql'}}, convert_article($article);
  }
}

# Save the output
foreach my $app (sort keys %archive) {
  log_msg('"' . $app . '":');

  # Create the dir?
  my $app_dir = $archive_dir . '/' . $app;
  if (! -e $app_dir) {
    mkdir $app_dir;
    log_msg('  (Creating archive dir)');
  }

  # Loop through the months
  foreach my $month (sort keys %{$archive{$app}}) {
    log_msg('  ' . $month . ': ' . scalar(@{$archive{$app}{$month}{'ids'}}) . ' article(s) to archive.');

    # Saving to file
    my $file = $app_dir . '/' . $month . '.log.gz';
    my $msg = join("\n", @{$archive{$app}{$month}{'sql'}});
    output_file($file, $msg);

    # And then actually archiving from the database
    foreach my $id (@{$archive{$app}{$month}{'ids'}}) {
      $del_sth->execute($app, $id);
    }
  }
}

# Re-base our IDs (at the start of each month)
my @now = localtime();
if ($now[3] == 1) {
  log_msg('*** Re-basing IDs ***');
  my $sth;

  # Temporary table management
  my @queries = (
    'DROP TEMPORARY TABLE IF EXISTS `tmp_NEWS_ARTICLES`;',
    'CREATE TEMPORARY TABLE `tmp_NEWS_ARTICLES` LIKE `NEWS_ARTICLES`;',
    'ALTER TABLE `tmp_NEWS_ARTICLES` DROP PRIMARY KEY, MODIFY COLUMN news_id SMALLINT UNSIGNED NULL;',

    # Copy across our existing articles
    'INSERT INTO `tmp_NEWS_ARTICLES` SELECT * FROM `NEWS_ARTICLES`;',
    'UPDATE `tmp_NEWS_ARTICLES` SET `news_id` = NULL;',

    # Restore in the live table
    'TRUNCATE `NEWS_ARTICLES`;',
    'INSERT INTO `NEWS_ARTICLES` SELECT * FROM `tmp_NEWS_ARTICLES`;',
  );
  foreach my $sql (@queries) {
    $sth = $dbh->prepare($sql)
      or warn_on_error('Unable to run ID re-basing query: ' . $DBI::errstr);
    $sth->execute;
  }
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log()
  if scalar(keys %archive);

##
## Sub-routines used above
##
sub convert_article {
  my ($article) = @_;

  my @ins_cols = (); my @ins_vals = ();

  foreach my $col (@cols) {
    push @ins_cols, '`' . $col . '`';

    my $val = defined($$article{$col}) ? '"' . $$article{$col} . '"' : 'NULL';
    push @ins_vals, $val;
  }

  return 'INSERT INTO `NEWS_ARTICLES` (' . join(', ', @ins_cols) . ') VALUES (' . join(', ', @ins_vals) . ');';
}
