#!/usr/bin/perl -w
# Perl config file
use DBI;
use POSIX;

our $base_dir;
our %config;
require "$base_dir/../config.pl";

# Database user needs to be the sysadmin user to cover all databases
$config{'db'}{'user'} = 'debearco_sysadmn';
# Pre-prepared database handles
$config{'db'}{'dbh'} = { };
$config{'db'}{'sth'} = { };

$config{'weather'} = {};
# OpenWeatherMap config
$config{'weather'}{'openweathermap'} = {
  'api_key'  => '',
  'uri'      => 'http://api.openweathermap.org/{endpoint}?{query_arg}={city}&mode=json&units=metric{extra}&APPID={api_key}',
  'arg_id'   => 'id',
  'arg_name' => 'q',
  'daily'    => {
    'days'     => 14,
    'endpoint' => 'data/2.5/forecast/daily',
    'extra'    => '&cnt=14', # The 14 represents the days argument above
  },
  'hourly'   => {
    'days'      => 5,
    'breakdown' => 3,
    'endpoint'  => 'data/2.5/forecast',
  },
  'current'  => {
    'endpoint' => 'data/2.5/weather',
  },
  'loaded'   => 0,
};

# Errors to log but not email
$config{'ignore_warnings'} = [ ];

# Log message
$config{'log_file'} = {
  'dir' => "$base_dir/../../logs/common/weather",
  'fmt' => '%Y-%m-%d',
  'ext' => 'log.gz',
};
$config{'log'} = '';

##
## Sub-routines
##
#
# Check if an argument we have been provided exists in the $opt object
#
sub is_opt {
  my ($arg_ref) = @_;
  my ($arg) = ($$arg_ref =~ m/^--(.+)$/);

  # This is a valid $opt argument, unless...
  return 0
    if !defined($arg)                # It does not begin with the dashes
       || !grep(/^$arg$/, keys %opt) # It does not exist in $opt
       || $opt{$arg};                # It has already been set

  # It's a valid argument, so update $arg_ref (so when we use it later it doesn't have the dashed start and return valid
  $$arg_ref = $arg;
  return 1;
}

#
# Establish where to save the log
#
sub setup_logging {
  my $log_date = strftime($config{'log_file'}{'fmt'}, localtime);
  $config{'log_file'} = "$config{'log_file'}{'dir'}/$log_date.$config{'log_file'}{'ext'}";
  $config{'log'} = "==========\n";
}

#
# Die (and log reason...) if an error occurred
#
sub die_on_error {
  my ($err) = @_;
  log_msg("ERROR - $err");
  push @errors, "E - $err";
  save_log();
  exit;
}

#
# Save a debug message
#
sub debug {
  my ($msg) = @_;
  log_msg($msg) if $opt{'debug'} || $opt{'verbose'};
}
sub verbose {
  my ($msg) = @_;
  log_msg($msg) if $opt{'verbose'};
}
sub warn {
  my ($err) = @_;
  log_msg("WARNING - $err");
}

#
# Check to see if a warning is common enough that we do not want to email it
#
sub ignore_warning {
  my ($err) = @_;
  return grep(/^$err$/, @{$config{'ignore_warnings'}});
}

#
# Record a log message with a timestamp
#
sub log_msg {
  my ($msg) = @_;
  $config{'log'} .= strftime('%Y-%m-%d %H:%M:%S', localtime) . " - $msg\n";
}

#
# Save the log to file
#
sub save_log {
  # If we're displaying to STDOUT instead, do so
  if ($opt{'no-save-log'} || $opt{'debug'} || $opt{'verbose'}) {
    print $config{'log'};
    return if $opt{'no-save-log'};
  }

  # The saving...
  output_file($config{'log_file'}, $config{'log'});

  # Determine script run as including last-level dir
  our $script_raw;
  my $script_dir = `basename \$(dirname $script_raw)`; chomp($script_dir);
  my $script = "$script_dir/$0";

  # If we have errors, email them to the Support Team
  send_email_queued(
    'bin_weather', 'error',
    "$script error" . (scalar(@errors) == 1 ? '' : 's'),
    'The following error' . (scalar(@errors) == 1 ? '' : 's') . " occurred during the current run:\n\n" . join("\n", @errors))
      if ($config{'live_server'} && scalar(@errors));
}

#
# Open a database connection
#
sub db_connect {
  my ($db_name) = @_;
  my $dbh = DBI->connect("dbi:mysql:$db_name:$config{'db'}{'host'}", $config{'db'}{'user'}, $config{'db'}{'pass'})
    or die_on_error("Unable to connect to DB '$db_name': " . $DBI::errstr);
  $dbh->do('SET time_zone = "Europe/London";')
    or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);
  return $dbh;
}

#
# Bespoke URI encode method
#
sub uri_encode {
  my ($str) = @_;
  my @str_cnv = '';

  # Handle undefined values
  return undef if !defined($str);

  # Convert to UTF-8 to handle UTF-8 chars
  my $debug = "Encoding string '$str' to ";
  utf8::encode($str);

  # Go through each character to see if it needs converting
  foreach my $char (split(//, $str)) {
    push @str_cnv,
      # 'Regular' character?
      ($char =~ m/^[a-z0-9\-\_\.]$/i
         ? $char

      # Something that needs converting
         : uc(sprintf('%%%x', ord($char))));
  }

  my $str_cnv = join('', @str_cnv);
  verbose("$debug'$str_cnv'");
  return $str_cnv;
}

# Return true to pacify the compiler
1;
