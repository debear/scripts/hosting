<?php

// Validate the arguments
if (
  count($argv) < 3
  || !preg_match('/^20\d{2}-[01]\d-[0-3]\d [0-2]\d:[0-5]\d:[0-6]\d$/', $argv[1])
  || !preg_match('/[a-z_]+\/[a-z_]+/i', $argv[2])
) {
  // Invalid input
  print "Invalid argument(s).\n";
  exit(1);
}
$arg_dt = $argv[1];
$arg_tz = $argv[2];

// Convert the datetime into UTC/GMT
$arg_utc = (new DateTime($arg_dt, new DateTimeZone($arg_tz)))
    ->setTimezone(new DateTimeZone('UTC'))
    ->format('Y-m-d H:i:s');

// Now convert the time from the requested timezone to ours
$server_tz = 'Europe/London';
if ($server_tz != $arg_tz) {
  $arg_dt = (new DateTime($arg_dt, new DateTimeZone($arg_tz)))
    ->setTimezone(new DateTimeZone($server_tz))
    ->format('Y-m-d H:i:s');
}

// Now determine time difference, in hours
$diff = (new DateTime())->diff(new DateTime($arg_dt));
$num_hours = (24 * $diff->days) + $diff->h + intval($diff->i >= 30);
printf('%s%d;%s', $num_hours && $diff->invert ? '-' : '', $num_hours, $arg_utc);
