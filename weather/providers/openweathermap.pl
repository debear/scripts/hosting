#!/usr/bin/perl -w
# OpenWeatherMap specific weather integration
use POSIX;
use JSON;
use Try::Tiny;
use HTML::Entities;
use Data::Dumper;

our %config;
our $browser;

# Prepare our statement for determining if an existing 'current' search has been made
my $sql = 'SELECT update_type FROM WEATHER_FORECAST WHERE app = ? AND instance_key = ?;';
$config{'db'}{'sth'}{'owm_current_type'} = $config{'db'}{'dbh'}{'common'}->prepare($sql)
  or die_on_error("Unable to prepare 'owm_current_type' statement: " . $DBI::errstr);
verbose("- Preparing Current Check SQL: $sql");

# Get the API key and apply to the URI
$config{'weather'}{'openweathermap'}{'api_key'} = do { local(@ARGV, $/) = "$config{'passwd_dir'}/api/weather/openweathermap"; <> }; chomp($config{'weather'}{'openweathermap'}{'api_key'});
$config{'weather'}{'openweathermap'}{'uri'} =~ s/\{api_key\}/$config{'weather'}{'openweathermap'}{'api_key'}/g;

# Main worker for OpenWeatherMap
$config{'weather'}{'openweathermap'}{'run'} = sub {
  my ($search, $matches) = @_;

  # Loop through each of the search matches
  my $num_updates = 0;
  foreach my $match (@$matches) {
    # Log details of what we found
    my $log_city = (defined($$match{'city_id'}) ? "ID $$match{'city_id'}" : "$$match{'city'}, $$match{'country'}");
    log_msg("- '$$match{'event_name'}' ($$match{'instance_key'}) :: '$log_city' on '$$match{'event_dt'}'");

    # Determine the timezone information for this match
    my $parsed_dt = `php $base_dir/parse_datetime.php '$$match{'event_dt'}' '$$match{'event_tz'}'`;
    # Producion fix: output includes a header we can't remove
    $parsed_dt =~ s/^Content-type: [^\n]+\s+//msi
      if $parsed_dt =~ /^Content-type: /;
    ($$match{'event_hours_until'}, $$match{'event_dt_utc'}) = split(/;/, $parsed_dt);
    if ($$match{'event_hours_until'} !~ /^-?\d+$/) {
      # An error occurred parsing the event, so flag and skip
      warn("  - Unable to parse '$$match{'event_dt'}' '$$match{'event_tz'}' into a number of hours until event - skipping");
      debug("  - '$$match{'event_hours_until'}'");
      next;
    }
    verbose("  - Event in $$match{'event_hours_until'} hour(s)");

    # If we already have a 'current' forecast, we can skip it
    $config{'db'}{'sth'}{'owm_current_type'}->execute($$search{'app'}, $$match{'instance_key'})
      or die_on_error("Unable to execute 'owm_current_type' statement with arguments '$$search{'app'}', '$$match{'instance_key'}': " . $DBI::errstr);
    my $cur_forecast = $config{'db'}{'sth'}{'owm_current_type'}->fetchrow_hashref;
    if (defined($cur_forecast) && $$cur_forecast{'update_type'} eq 'current') {
      log_msg("  - Already have the 'current' forecast");
      next;
    }

    # Determine the 'type' of lookup (i.e., granularity)
    $$match{'search_type'} = openweathermap_type($match);
    log_msg("  - Using '$$match{'search_type'}' forecast");

    # Download
    my $request = openweathermap_get($match);
    next if !defined($request);

    # Get the appropriate item in the list
    my $list = openweathermap_identify($request, $match);
    verbose("  - List returned: " . Dumper($list));

    # If there was an error during an huorly check, fall-back to daily mode
    if (!defined($list) && ($$match{'search_type'} eq 'hourly')) {
      log_msg("  - No hourly list found, falling back to daily mode");
      $$match{'search_type'} = 'daily';

      # Re-download
      $request = openweathermap_get($match);
      next if !defined($request);

      # Re-parse the list
      $list = openweathermap_identify($request, $match);
      verbose("    - List returned: " . Dumper($list));
    }
    # If still errored, warn
    if (!defined($list)) {
      warn("  - No list found");
      next;
    }

    # All good, carry on...
    my %forecast = (
      'symbol'      => $$list{'weather'}[0]{'icon'},
      'summary'     => $$list{'weather'}[0]{'main'},
      'description' => $$list{'weather'}[0]{'description'},
    );
    # Get the current weather / An hourly(ish) update
    if ($$match{'search_type'} ne 'daily') {
      $forecast{'temp'}          = $$list{'main'}{'temp'};
      $forecast{'pressure'}      = $$list{'main'}{'pressure'};
      $forecast{'precipitation'} = $$list{'pop'};
      $forecast{'humidity'}      = $$list{'main'}{'humidity'};
      $forecast{'clouds'}        = $$list{'clouds'}{'all'};
      $forecast{'wind_speed'}    = $$list{'wind'}{'speed'};
      $forecast{'wind_dir'}      = $$list{'wind'}{'deg'};

    # A single summary for the day
    } else {
      my $temp_key = openweathermap_temp_key($$match{'event_dt'});
      $forecast{'temp'}          = $$list{'temp'}{$temp_key};
      $forecast{'pressure'}      = $$list{'pressure'};
      $forecast{'precipitation'} = $$list{'pop'};
      $forecast{'humidity'}      = $$list{'humidity'};
      $forecast{'clouds'}        = $$list{'clouds'};
      $forecast{'wind_speed'}    = $$list{'speed'};
      $forecast{'wind_dir'}      = $$list{'deg'};
    }

    # Get the array to store the details
    my @sql_args = (
      $$search{'app'},
      $$match{'instance_key'},
      $$match{'event_dt'},
      openweathermap_symbol($forecast{'symbol'}),
      $forecast{'symbol'},
      $forecast{'summary'},
      openweathermap_description($forecast{'description'}),
      $forecast{'temp'},
      $forecast{'pressure'},
      $forecast{'precipitation'},
      $forecast{'humidity'},
      $forecast{'clouds'},
      $forecast{'wind_speed'},
      openweathermap_wind_dir($forecast{'wind_dir'}),
      $forecast{'wind_dir'},
      $$match{'search_type'},
    );
    debug("  - Saving as: " . Dumper(\@sql_args));
    $config{'db'}{'sth'}{'insert'}->execute(@sql_args)
      or die_on_error("Unable to execute 'insert' for '$$search{'app'}', '$$match{'instance_key'}': " . $DBI::errstr);
    $num_updates++;
  }

  # Confirm the number of new entries
  return $num_updates;
};

# Determine the type of request we need to be looking through
sub openweathermap_type {
  my ($match) = @_;

  # If we're a previous day, or before the advertised time, we're current
  if ($$match{'event_hours_until'} <= 0) {
    # Use the current forecast
    verbose('  - Event is happening or has happend');
    return 'current';
  } elsif ($$match{'event_hours_until'} <= (24 * $config{'weather'}{'openweathermap'}{'hourly'}{'days'})) {
    # Use the hourly forecast
    verbose('  - Event is within the \'hourly\' range');
    return 'hourly';
  } else {
    # Use the longer range daily forecast
    verbose('  - Fallback to the longer range \'daily\' forecast');
    return 'daily';
  }
}

# Adjust country codes from what we have in the database to a legitimate code
sub openweathermap_country {
  my ($country) = @_;

  # A country and region
  if ($country =~ m/-/) {
    my ($l, $r) = split('-', $country);
    return $l;

  # Polish SGPs including town/city code in code
  } elsif ($country =~ m/^pl/i) {
    return 'pl';

  # Scandanvian or European cities grouped by group of countries, not individual
  } elsif ($country =~ m/^(eu|sca|ndc)$/i) {
    return '';
  }

  # Standard input needs to remain unchanged
  return $country;
}

# Get the forecast
sub openweathermap_get {
  my ($match) = @_;

  # URLEncode the name, but database decode (remove HTML codes) it first
  # e.g., M&aring;lilla -> Målilla -> M%c3%a5lilla
  $$match{'city'} = uri_encode(decode_entities($$match{'city'}))
    if defined($$match{'city'});
  # Some country codes need modifying from the app's purpose
  $$match{'country'} = openweathermap_country($$match{'country'})
    if defined($$match{'country'});

  # What's the URI we call?
  my %cfg = %{$config{'weather'}{'openweathermap'}};
  $$match{'uri'} = $cfg{'uri'};
  debug("  - Base URI: $$match{'uri'}");
  $$match{'uri'} =~ s/{endpoint}/$cfg{$$match{'search_type'}}{'endpoint'}/;
  if (defined($$match{'city_id'})) {
    # Convert the query to a by-ID lookup
    $$match{'uri'} =~ s/{query_arg}/$cfg{'arg_id'}/;
    $$match{'uri'} =~ s/{city}/$$match{'city_id'}/;
  } else {
    $$match{'uri'} =~ s/{query_arg}/$cfg{'arg_name'}/;
    $$match{'uri'} =~ s/{city}/$$match{'city'},$$match{'country'}/;
  }
  my $extra = defined($cfg{$$match{'search_type'}}{'extra'}) ? $cfg{$$match{'search_type'}}{'extra'} : '';
  $$match{'uri'} =~ s/{extra}/$extra/;
  debug("  - Request URI: $$match{'uri'}");

  # Download
  my $request_obj;
  my $ret = $browser->get($$match{'uri'});
  debug("  - LWP Return Object: " . Dumper($ret));
  log_msg("  - LWP Return Object: " . Dumper($ret)) if $opt{'save-request'} && !$opt{'debug'} && !$opt{'verbose'};

  # Check for a 510 error we get periodicaly
  if ($ret->code == 510) {
    warn("  - HTTP Code 510 returned from $$match{'uri'}");
    return undef;
  }

  my $request_txt = $ret->decoded_content;
  if (!defined($request_txt) || $request_txt eq '') {
    warn("  - Empty response from $$match{'uri'}");
    return undef;
  }
  debug("  - Request from $$match{'uri'}:\n$request_txt");
  log_msg("  - Request: $request_txt") if $opt{'save-request'} && !$opt{'debug'} && !$opt{'verbose'};

  # Work around some known nginx issues
  if ($request_txt =~ m/<h1>500 Internal Server Error<\/h1>/) {
    # 500 Internal Server Error, in a 200 page
    warn("  - nginx 500 error from $$match{'uri'}");
    return undef;

  } elsif ($request_txt =~ m/failed to set keepalive/) {
    # "failed to set keepalive"
    warn("  - Keepalive error from $$match{'uri'}");
    return undef;
  }

  # Convert text to a JSON object
  try {
    $request_obj = decode_json($request_txt);
  } catch {
    warn("  - Unable to decode JSON from $$match{'uri'}");
    return undef;
  };

  # All okay
  return $request_obj;
}

# Breakdown the forecast request into the parts we need
sub openweathermap_identify {
  my ($request, $match) = @_;
  debug("  - Identify Record: '$$match{'search_type'}', '$$match{'event_dt'}', (UTC: '$$match{'event_dt_utc'}')");

  # Current weather is avaiable as-is
  return $request if $$match{'search_type'} eq 'current';

  # Hourly - match the date and nearest hourly division
  if ($$match{'search_type'} eq 'hourly') {
    # Need to convert our date/time to a ts for comparison
    my ($date, $time) = split(' ', $$match{'event_dt_utc'});
    my @date_s = split('-', $date);
    my @time_s = split(':', $time);
    my $ts = mktime($time_s[2], $time_s[1], $time_s[0], $date_s[2], $date_s[1]-1, $date_s[0]-1900);
    debug("      - Converted '$$match{'event_dt_utc'}' to '$ts'");

    # And how far we need to be from the division (half the hourly breakdown)
    my $offset = $config{'weather'}{'openweathermap'}{'hourly'}{'breakdown'} * 1800; # 1800 = (60 * 60) / 2 = Seconds -> Hours, halved

    foreach my $list (@{$$request{'list'}}) {
      my $test_offset = abs($$list{'dt'} - $ts);
      my $cmp = ($test_offset < $offset);
      my @test = gmtime($$list{'dt'});
      debug(sprintf("    - List: %s (%04d-%02d-%02d %02d:%02d:%02d) => %s => %s",
        $$list{'dt'},
        $test[5] + 1900,
        $test[4] + 1,
        $test[3],
        $test[2],
        $test[1],
        $test[0],
        $test_offset,
        $cmp ? 'Y' : 'N'));

      return $list if $cmp;
    }

  # Daily is a bit easier - just matching the date in the list
  } elsif ($$match{'search_type'} eq 'daily') {
    foreach my $list (@{$$request{'list'}}) {
      my $test_date = strftime('%Y-%m-%d', gmtime($$list{'dt'}));
      my $cmp = ($test_date eq substr($$match{'event_dt_utc'}, 0, 10));
      debug("    - List: $test_date => " . ($cmp ? 'Y' : 'N'));
      return $list if $cmp;
    }
  }

  # Nothing matched!?
  warn("    - No matches found!");
  return undef;
}
# Establish the appropriate time of day
sub openweathermap_temp_key {
  my ($datetime) = @_;
  return 'max' if !defined($datetime);

  my $h = substr($datetime, 11, 2);
  my $tod = 'max';
  if ($h < 6 || $h > 20) {
    # Pre-6am or Post-9pm
    $tod = 'night';
  } elsif ($h < 12) {
    # Between 6am and Midday
    $tod = 'morn';
  } elsif ($h < 18) {
    # Between Midday and 6pm
    $tod = 'day';
  } else {
    # Between 6pm and 9pm
    $tod = 'eve';
  }

  verbose("Converting hour '$h' to '$tod'");
  return $tod;
}

# Convert the symbol from the APIs code to our internal version
# See: https://openweathermap.org/weather-conditions
sub openweathermap_symbol {
  my ($symbol) = @_;
  my ($code, $time) = ($symbol =~ m/^(..)(.)$/i);
  my $icon = '';

  # Icons: clouds, cloudy, lightning, moon, rain, snow, sun
  # Meaning of codes (time of day, d = day, n = night)
  # 01 - Clear
  if ($code eq '01') {
    $icon = ($time eq 'd' ? 'sun' : 'night');
  # 02 - Few clouds (day)
  } elsif ($code eq '02' && $time eq 'd') {
    $icon = 'cloudy';
  # 02 - Few clouds (night)
  # 03 - Scattered clouds
  # 04 - Broken clouds
  } elsif ($code =~ m/^02|03|04$/) {
    $icon = 'clouds';
  # 09 - Shower rain
  # 10 - Rain
  } elsif ($code =~ m/^09|10$/) {
    $icon = 'rain';
  # 11 - Thunderstorm
  } elsif ($code eq '11') {
    $icon = 'lightning';
  # 13 - Snow
  } elsif ($code eq '13') {
    $icon = 'snow';
  # 50 - Mist
  } else {
    $icon = 'mist';
  }

  verbose("Converting symbol '$symbol' (Code '$code', Time '$time') to icon '$icon'");
  return $icon;
}

# Convert the case of the description
sub openweathermap_description {
  my ($desc) = @_;
  return (defined($desc) ? ucfirst($desc) : undef);
}

# Convert the direction as an angle into a two-char code
sub openweathermap_wind_dir {
  my ($angle) = @_;
  return undef if !defined($angle);

  # Divided in to 8 dirs, 45deg wide each (to which we apply an offset, e.g., 157.5..202.5deg => S)
  my $code = '';
  if ($angle <= 22.5 || $angle > 337.5) {
    $code = 'N';
  } elsif ($angle <= 67.5) {
    $code = 'NE';
  } elsif ($angle <= 112.5) {
    $code = 'E';
  } elsif ($angle <= 157.5) {
    $code = 'SE';
  } elsif ($angle <= 202.5) {
    $code = 'S';
  } elsif ($angle <= 247.5) {
    $code = 'SW';
  } elsif ($angle <= 292.5) {
    $code = 'W';
  } elsif ($angle <= 337.5) {
    $code = 'NW';
  }

  verbose("Converting wind direction '$angle' deg to direction '$code'");
  return $code;
}

# Return true to pacify the compiler
1;
