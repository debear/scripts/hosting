#!/usr/bin/perl -w
# Download and import weather forecast from a REST API

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, "$xilo_home/$xilo_custom_path");
  }
}

use strict;
use utf8;
use LWP::UserAgent;
use Data::Dumper;

# Mask the filename as we're on a shared server
our $base_dir;
our $script_raw = $0;
$0 = basename $0;

# Get base config
our %config;
require "$base_dir/config.pl";

# Logging info
setup_logging();

# Validate arguments
our %opt = (
  'debug'        => 0, # Only get the _list_ of news items, do not store in the database
  'verbose'      => 0, # Increased debug
  'no-save-log'  => 0, # Display the log on STDOUT, not to file
  'save-request' => 0, # Save the request we have been passed
);

foreach my $arg (@ARGV) {
  # Is it an option we're looking for?
  if (is_opt(\$arg)) {
    log_msg("* Running in '$arg' mode *");
    $opt{$arg} = 1;

  # Not expecting anything, so log we don't know what to do with the argument, and ignore it
  } else {
    log_msg("* Unknown argument '$arg'");
  }
}

# Create our LWP object that will interrogate the API
our $browser = new LWP::UserAgent;
$browser->agent('WeatherSync/2.0');

# Connect to the database
$config{'db'}{'dbh'}{'common'} = db_connect($config{'db'}{'name'});

# Prepare our statement for saving the results
my $sql = 'INSERT INTO WEATHER_FORECAST (app, instance_key, forecast_date, symbol, symbol_raw, summary, description, temp, pressure, precipitation, humidity, clouds, wind_speed, wind_dir, wind_dir_raw, update_type, last_updated)
  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())
ON DUPLICATE KEY UPDATE symbol = VALUES(symbol), symbol_raw = VALUES(symbol_raw), summary = VALUES(summary), description = VALUES(description), temp = VALUES(temp),
                        pressure = VALUES(pressure), precipitation = IFNULL(VALUES(precipitation), precipitation), humidity = VALUES(humidity), clouds = VALUES(clouds), wind_speed = VALUES(wind_speed),
                        wind_dir = VALUES(wind_dir), wind_dir_raw = VALUES(wind_dir_raw), update_type = VALUES(update_type), last_updated = VALUES(last_updated);';
$config{'db'}{'sth'}{'insert'} = $config{'db'}{'dbh'}{'common'}->prepare($sql)
  or die_on_error("Unable to prepare 'insert' statement: " . $DBI::errstr);
verbose("Inserting SQL: $sql");

# Get the forecast searches
$sql = 'SELECT app, search_id, display_name, database_name, search, provider
FROM WEATHER_SEARCHES
WHERE active = 1;';
my $searches = $config{'db'}{'dbh'}{'common'}->selectall_arrayref($sql, { Slice => {} })
  or die_on_error("Unable to get the list of searches: " . $DBI::errstr);
verbose("Searches: " . Dumper($searches));

# Skip if no matches...
@$searches = ( ) if !defined($searches);
# Otherwise, loop through them
my $num_updates = 0;
foreach my $search (@$searches) {
  log_msg("Search: '$$search{'display_name'}' ($$search{'app'} // $$search{'search_id'}) via '$$search{'provider'}'");

  # Is this a known provider?
  if (!defined($config{'weather'}{$$search{'provider'}})) {
    warn("- Unknown provider?");
    next;

  # Do we need to load the config file (dynamic, just-in-time, approach)
  } elsif (!$config{'weather'}{$$search{'provider'}}{'loaded'}) {
    verbose("- First instance of $$search{'provider'}, loading the provider code");
    require "$base_dir/providers/$$search{'provider'}.pl";
    $config{'weather'}{$$search{'provider'}}{'loaded'} = 1;
  }

  # Do we need to establish a database connection?
  if (!defined($config{'db'}{'dbh'}{$$search{'database_name'}})) {
    my $db_name = "debearco_$$search{'database_name'}";
    verbose("- Opening database connection to $db_name");
    $config{'db'}{'dbh'}{$$search{'database_name'}} = db_connect($db_name);
  }

  # Run the query to get the data
  my $sql = $$search{'search'};
  my @sql_args = ();
  foreach my $s ($sql =~ /\{([^\}]+)\}/g) {
    my $val = $config{'weather'}{$$search{'provider'}};
    foreach my $cfg (split '\|', $s) {
      next if $cfg eq 'cfg';
      $val = $$val{$cfg};
    }
    $sql =~ s/\{\Q$s\E\}/?/g;
    push @sql_args, $val;
  }
  debug("- SQL: $sql");
  debug("- Args: " . Dumper(\@sql_args));
  my $matches = $config{'db'}{'dbh'}{$$search{'database_name'}}->selectall_arrayref($sql, { Slice => {} }, @sql_args)
    or die_on_error("Unable to get the list of matches for '$$search{'display_name'}' ($$search{'app'} // $$search{'search_id'}): " . $DBI::errstr);
  if (!@$matches) {
    log_msg("- No results returned");
    next;
  }

  # Perform the search
  $num_updates += $config{'weather'}{$$search{'provider'}}{'run'}->($search, $matches);
}
if (!$num_updates) {
  log_msg("No forecasts applied");
} else {
  log_msg("$num_updates total forecast(s) applied");
}

# Disconnect from the databases
foreach my $h (keys %{$config{'db'}{'sth'}}) {
  next if !defined($config{'db'}{'sth'}{$h});
  verbose("Closing statement handle '$h'");
  $config{'db'}{'sth'}{$h}->finish;
}
foreach my $h (keys %{$config{'db'}{'dbh'}}) {
  next if !defined($config{'db'}{'dbh'}{$h});
  verbose("Closing database handle '$h'");
  $config{'db'}{'dbh'}{$h}->disconnect;
}

# Save the log to file and then exit
save_log();
