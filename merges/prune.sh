#!/bin/bash
# Merged CSS/JS pruner

# Base config
here=`dirname $0`
today=`date +'%F'`
log_file=$here/../../logs/common/merges/$today.log.gz

# Helpers
. $here/../common/log.sh

# Define our TTLs
declare -A ttls
# Leave anything under a month old for our CSS/JS
ttls[css]=30
ttls[js]=30
# Whilst views last 90 days
ttls[views]=90

# Where and what to search for
base_path_dev=/var/www/debear/merges
base_path_live=~/debear/merges

# State we've started
log '============='
log 'Run Starting.'

# Loop through the various types
for type in css js views
do
    log
    log "Processing '$type'"
    glob_path="*/$type"
    ttl=${ttls[$type]}

    # Determine which server we're on to find out which base_path to use
    if [ $(hostname | grep -c '\.xnoc\.net$') -eq 1 ]
    then
        is_dev=0
        base_path=$base_path_live
    else
        is_dev=1
        base_path=$base_path_dev
    fi

    # Log our vars
    log "- Config:"
    log "  - TTL: $ttl"
    log "  - Is Dev: $is_dev"
    log "  - Path: $base_path"
    log "  - Path Glob: $glob_path"

    # Find the file(s)
    cd $base_path
    log '- Running:'
    raw_list=`find $glob_path -type f -mtime +$ttl 2>/dev/null`
    prune_list=($raw_list)
    log "  - ${#prune_list[@]} result(s) found."

    # Any results? If not no need to continue...
    if [ ${#prune_list[@]} -eq 0 ]
    then
        continue
    fi

    # Log which files we're pruning
    log '- The following files will be pruned:'
    for file in ${prune_list[@]}
    do
    	log "  - $file"
    done

    # Run
    rm -rf ${prune_list[@]}
done

# Log the end
log
log 'Run Completed!'
