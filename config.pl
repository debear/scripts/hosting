#!/usr/bin/perl -w
# Global perl config file

use MIME::Base64;
use POSIX qw/strftime/;

# Configuration info
our $base_dir;
our %config = ( );

# Common includes
require "$base_dir/../common/io.pl";
require "$base_dir/../common/email.pl";

# Determine which server we're running on
$config{'live_server'} = ($base_dir =~ /^\/var\/www\/f6a0812f-78d0-45a6-bec0-c060de245116/);
$config{'passwd_dir'} = '/var/www' . ($config{'live_server'} ? '/f6a0812f-78d0-45a6-bec0-c060de245116' : '') . '/debear/etc/passwd';

# Database connection config
$config{'db'} = {
  'host' => ($config{'live_server'} ? 'localhost' : 'db.debear.dev'),
  'name' => 'debearco_common',
  'user' => 'debearco_admin',
  'pass' => '',
};
# Password
$config{'db'}{'pass'} = do { local(@ARGV, $/) = "$config{'passwd_dir'}/web/db"; <> }; chomp($config{'db'}{'pass'});
$config{'db'}{'pass'} = decode_base64($config{'db'}{'pass'});

# Email contact info
$config{'email'} = {
  'sendmail' => '/usr/lib/sendmail -oi -t -f noreply@debear.uk',
  'from'     => '"DeBear Support" <noreply@debear.uk>',
  'to'       => '"DeBear Support" <support@debear.uk>',
};
