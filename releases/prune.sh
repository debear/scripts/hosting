#!/bin/bash
# Parse old and unlinked release directories

# Base config
dir_bin="$(dirname $0)/.."
dir_base="$dir_bin/.."
dir_sites="$dir_base/sites"
if [ "$(basename $dir_base)" = 'bin' ]
then
    # We need an extra level on dev
    dir_base=$(dirname $dir_base)
    dir_sites='/sshfs/debear/sites'
fi
log_file="$dir_base/logs/common/releases-prune/$(date +'%F').log.gz"

# Helpers
. $dir_bin/common/log.sh

# Determine the releases
cd $dir_sites
for repo_dir in $(ls -d * | grep -P '_[0-9a-f]{40}$')
do
    # From this the repo and whether or not it's linked
    repo=$(echo "$repo_dir" | sed -r 's/_[0-9a-f]{40}$//')
    log "$repo_dir (Repo: $repo)"

    # Is it the repo's current link?
    repo_curr=$(basename $(readlink -f $repo))
    if [ "x$repo_curr" = "x$repo_dir" ]
    then
        log "- Skipping: This is the current release."
        continue
    fi

    # Sufficiently old to be pruned?
    prunable=$(find * -maxdepth 1 -type d -name $repo_dir -mtime +1 | wc -l)
    if [ $prunable -eq 0 ]
    then
        # Not >24 hours old, so no
        log "- Skipping: Created within the last 24hrs, too recently for pruning."
        continue
    fi

    # All good to prune...
    log "- Pruning."
    rm -rf $repo_dir
done
