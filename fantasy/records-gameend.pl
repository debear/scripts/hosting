#!/usr/bin/perl -w
# Fantasy Record Breakers Game-End Email Script

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, "$xilo_home/$xilo_custom_path");
  }
}

use strict;
use POSIX qw/strftime/;

# Mask the filename as we're on a shared server
our $base_dir;
our $script_raw = $0;
$0 = basename $0;

# Get base config
our %config;
require "$base_dir/../config.pl";

# Run the artisan command to perform the processing
my $cmd = '/usr/bin/php ~/debear/sites/_debear/artisan debear:fantasy:records-email-gameend';
my $log_date = strftime('%Y-%m-%d', localtime);
my $log_file = "~/debear/logs/fantasy/scripts-records_gameend/$log_date.log.gz";
my $retstat = system("bash", "-o", "pipefail", "-c", "$cmd | gzip >>$log_file");
my $retcode = $retstat >> 8;

# If this failed, send a notification
send_email_queued(
  'fantasy_records', 'game_end_error',
  'Automated Record Breakers game-end email script error',
  "A non-zero exit code ('$retcode') was returned by the artisan script which will need reviewing."
) if $retcode > 0;

# Propagate the artisan script's code
exit $retcode
