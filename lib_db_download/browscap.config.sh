# Browscap specific config
browscap_vendor_dir="/var/www/debear/sites/_debear/vendor"
browscap_vendor_path="browscap/browscap-php"
browscap_local_dir="$browscap_vendor_dir/$browscap_vendor_path"
browscap_local_src="resources"
browscap_script="bin/browscap-php"

browscap_sshfs="/sshfs/debear/sites/_debear/vendor"
browscap_sshfs_dir="$browscap_sshfs/$browscap_vendor_path"

browscap_cidata_dir="/var/www/debear/ci-data/files/_global/master/browscap"
browscap_cidata="$browscap_cidata_dir/$browscap_local_src.tar.gz"
