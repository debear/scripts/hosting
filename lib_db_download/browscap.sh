#!/bin/bash
# Download and "install" the latest Browscap database file
# TD. 2018-02-20.
exec 2>&1

#
# Base config
#
here=`dirname $0`
script=browscap

# Load config / helpers
. "$here/config.sh"
. "$here/$script.config.sh"

# Setup
setup_logging

# Cannot run on live only
if [ $(is_live) -eq 1 ]
then
  log_error "This script cannot be run on live."
fi

# Run the update script locally
log "Running update script $browscap_local_dir/$browscap_script with argument 'browscap:update'."
sudo $browscap_local_dir/$browscap_script browscap:update

log "Fixing file permissions."
sudo chown -R apache:apache "$browscap_local_dir/$browscap_local_src"

# Copy to production
if [ $(is_data) -eq 1 ]
then
  # Make a copy for our CI Data
  cd $browscap_local_dir
  tar -czf $browscap_cidata $browscap_local_src
  cd $browscap_cidata_dir
  ./.cache.build
else
  log "Skipping sync to live whilst running on dev box."
fi

log "Complete"
