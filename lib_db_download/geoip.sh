#!/bin/bash
# Download and "install" the latest GeoIP database file
# TD. 2018-02-20.
exec 2>&1

#
# Base config
#
here=`dirname $0`
script=geoip
debug=0

# Load config / helpers
. "$here/config.sh"
. "$here/$script.local.sh"
. "$here/$script.config.sh"

# Setup
setup_logging

# Cannot run on live only
if [ $(is_live) -eq 1 ]
then
  log_error "This script cannot be run on live."
fi

# Download, first to temp
cd $geoip_tmp
geoip_local_file="${geoip_file}.tar.gz"
if [ $debug -eq 0 ]
then
  if [ -e $geoip_local_file ]
  then
    log "Removing existing temporary downloaded file."
    rm $geoip_local_file
  fi
  log "Downloading from '$geoip_remote' to '$geoip_tmp/$geoip_local_file'."
  wget -qO$geoip_local_file "$geoip_remote"
else
  log "Download skipped in 'debug' mode."
fi

# Download okay?
if [ ! -e $geoip_remote_file ] || [ ! -s $geoip_remote_file ]
then
  log_error "An error occurred whilst downloading the remote file."
fi

# Extract the file
log "Extracting to '$geoip_local'."
tar -xzf $geoip_local_file --strip-components 1 --wildcards-match-slash --wildcards "${geoip_file}_*/$geoip_file.mmdb"
# Then copy to the destination folder
cp -p $geoip_file.mmdb $geoip_local

# Copy to production
if [ $(is_data) -eq 1 ]
then
  # Copy to live
  log "Syncing to live."
  if [ $(df -h /sshfs | grep -Fc '/sshfs') -eq 0 ]
  then
    sshfs-debear
  fi
  rsync -qrlptDz $geoip_local $geoip_sshfs

  # But then also a copy for our CI Data
  gzip -c $geoip_local >$geoip_cidata
  cd $geoip_cidata_dir
  ./.cache.build
else
  log "Skipping sync to live whilst running on dev box."
fi

log "Complete"
