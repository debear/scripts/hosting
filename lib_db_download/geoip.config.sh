# GeoIP specific config
geoip_file='GeoLite2-City' # -Country when lat/long dropped
geoip_remote="https://download.maxmind.com/app/geoip_download?edition_id=$geoip_file&license_key=$geoip_maxmind_key&suffix=tar.gz"
geoip_local="/var/www/debear/sites/common/geoip/$geoip_file.mmdb"
geoip_cidata_dir="/var/www/debear/ci-data/files/_global/master/geoip"
geoip_cidata="$geoip_cidata_dir/$geoip_file.mmdb.gz"
geoip_sshfs="/sshfs/debear/sites/common/geoip"
geoip_tmp='/tmp'
