# Global config and methods
today=$(date +'%F')
log_dir=/var/www/debear/logs/common

#
# Host info
#
host=$(hostname)

function is_live() {
  if [ $(echo "$host" | grep -c '\.xnoc\.net$') -eq 1 ]
  then
    echo 1
  else
    echo 0
  fi
}
function is_data() {
  if [ "x$host" = 'xmilou' ]
  then
    echo 1
  else
    echo 0
  fi
}
function is_dev() {
  if [ "x$host" = 'xtintin' ]
  then
    echo 1
  else
    echo 0
  fi
}

#
# Logging
#
function setup_logging() {
  log_dir="$log_dir/lib_db-$script"
  mkdir -p $log_dir
  log "------------------------------"
}

function log() {
  log_worker 'INFO' "$1"
}
function log_warn() {
  log_worker 'WARN' "$1"
}
function log_error() {
  log_worker 'ERROR' "$1"
  exit 1
}

function log_worker() {
  echo `date +'[%F %T]'` [$1] $2 | gzip >>$log_dir/$today.log.gz
}
