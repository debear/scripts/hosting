#!/usr/bin/perl -w
# Twitter config

##
## General config
##
our %config;
$config{'debug'} = 0; # Three state debug flag: 0 = live, 1 = dry run, 2 = run with test credentials
$config{'twitter'} = {
  'user_agent' => {
    'name' => 'DeBear Twitter Poster',
    'version' => 1.3,
    'url' => 'https://gitlab.com/debear',
  },
  'test' => {
    'app' => 'dev',
    'account_pri' => 'DeBearTestA', 'account_pri_int' => 369471592,
    'account_sec' => 'DeBearTestB', 'account_sec_int' => 2154264192,
  },
  'apps' => { }, 'users' => { }
};
$config{'key_dir'} = $config{'passwd_dir'} . '/api/twitter';

##
## Key Loading
##
sub load_key {
  my ($app, $acct) = @_;
  if (!defined($config{'twitter'}{$app}{$acct})) {
    my $keys = do { local(@ARGV, $/) = "$config{'key_dir'}/$app/$acct"; <> }; chomp($keys);
    my @s = split("\n", $keys);
    $config{'twitter'}{$app}{$acct} = {
      ($acct eq '_app' ? 'key' : 'token') => $s[0],
      'secret' => $s[1],
    };
  }
  return $config{'twitter'}{$app}{$acct};
}

##
## Create a client
##
sub create_twitter_client {
  my ($app, $user) = @_;
  our %config;
  my %ua = %{$config{'twitter'}{'user_agent'}};

  return Twitter::API->new_with_traits(
    traits => [ qw/Migration ApiMethods/ ],

    # Twitter API config
    api_url    => 'https://api.x.com',
    upload_url => 'https://upload.twitter.com',

    # User Agent
    agent           => "$ua{'name'}/$ua{'version'}",
    default_headers => {
      x_twitter_client_version => $ua{'version'},
      x_twitter_client_url     => $ua{'url'},
    },

    # Tweet config
    consumer_key        => $$app{'key'},
    consumer_secret     => $$app{'secret'},
    access_token        => $$user{'token'},
    access_token_secret => $$user{'secret'},
  );
};

# Return true to pacify the compiler
1;
