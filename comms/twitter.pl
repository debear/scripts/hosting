#!/usr/bin/perl -w
# Send all queued tweets marked to be sent

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $xilo_is = 0;
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $xilo_is = 1;
  }
}

use strict;
use utf8;
use DBI;
use Twitter::API;
use Scalar::Util qw(blessed);
use HTML::Entities;
use JSON;
use Try::Tiny;
use Data::Dumper; # Not added for testing, used for dumping return info

# Mask the filename as we're on a shared server
our $base_dir;
$0 = basename $0;

# Get base config, plus our list of Twitter accounts
our %config; our $xilo_is;
require $base_dir . '/config.pl';
require $base_dir . '/twitter.config.pl';

# Logging info
setup_logging();

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
$dbh->do('SET time_zone = "Europe/London";')
  or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);

# Prepare the SQL statements
my $tweet_detail_sth = $dbh->prepare('SELECT tweet_body, twitter_tweet_id FROM COMMS_TWITTER WHERE app = ? AND tweet_id = ?;');
my $tweet_update_sth = $dbh->prepare('UPDATE COMMS_TWITTER SET tweet_sent = NOW(), tweet_status = ?, twitter_tweet_id = ?, twitter_return_code = ?, twitter_return_response = COMPRESS(?), twitter_return_info = COMPRESS(?) WHERE app = ? AND tweet_id = ?;');
# Media statements (expiry compared to a minute in the future to allow for future processing time)
my $media_sth = $dbh->prepare('SELECT app, tweet_id, media_id, media_local, media_unique_ref, twitter_media_id, media_expires, IFNULL(media_expires, "2099-12-31 23:59:59") < DATE_ADD(NOW(), INTERVAL 1 MINUTE) AS media_expired FROM COMMS_TWITTER_MEDIA WHERE app = ? AND tweet_id = ? ORDER BY media_order;');
my $media_update_sth = $dbh->prepare('UPDATE COMMS_TWITTER_MEDIA SET media_uploaded = NOW(), media_expires = IF(? = 1, DATE_ADD(NOW(), INTERVAL ? SECOND), NULL), twitter_media_id = ?, twitter_media_key = ?, twitter_return_code = ?, twitter_return_response = COMPRESS(?), twitter_return_info = COMPRESS(?) WHERE app = ? AND tweet_id = ? AND media_id = ?;');

# Import messages that are to be synced to the sending pool
import_sync();

# Twitter clients
my %client = ( );

# Get the list of tweets to send
my @queue = @{ $dbh->selectall_arrayref('SELECT app, tweet_id, retweet_id, twitter_app, twitter_account FROM COMMS_TWITTER WHERE tweet_send <= NOW() AND tweet_status = "queued" ORDER BY tweet_send, app, tweet_id;', { Slice => {} }) };
if (@queue) {
  LOOP_TWEET: foreach my $tweet (@queue) {
    my $is_retweet = defined($$tweet{'retweet_id'});
    log_msg("Sending tweet $$tweet{'app'} / $$tweet{'tweet_id'}" . ($is_retweet ? " / $$tweet{'retweet_id'}" : '') . " (from app $$tweet{'twitter_app'}, user $$tweet{'twitter_account'})");

    # First, get the tweet details
    $tweet_detail_sth->execute($$tweet{'app'}, !$is_retweet ? $$tweet{'tweet_id'} : $$tweet{'retweet_id'});
    my $details = $tweet_detail_sth->fetchrow_hashref;
    $$details{'tweet_body'} = decode_entities($$details{'tweet_body'});

    # Retweeting a Tweet that failed?
    if ($is_retweet && (!defined($$details{'twitter_tweet_id'}) || !$$details{'twitter_tweet_id'})) {
      update_tweet_status($tweet, { 'ret_resp' => "Unable to retweet a failed tweet" });
      next LOOP_TWEET;
    }

    # If testing, use alternate user details
    if (!$xilo_is || ($config{'debug'} == 2)) {
      $$tweet{'twitter_app'} = $config{'twitter'}{'test'}{'app'};
      $$tweet{'twitter_account'} = $config{'twitter'}{'test'}{'account_' . (!$is_retweet ? 'pri' : 'sec')};
      log_msg("  In test-credential debug mode, switching app to '$$tweet{'twitter_app'}' and '$$tweet{'twitter_account'}'");
    }

    # Then the keys and tokens
    my $app = load_key($$tweet{'twitter_app'}, '_app');
    my $user = load_key($$tweet{'twitter_app'}, $$tweet{'twitter_account'});

    # Validate these keys and tokens, throw error and skip if invalid
    if (!defined($$app{'key'}) || !defined($$app{'secret'})) {
      update_tweet_status($tweet, { 'ret_resp' => "Invalid app '$$tweet{'twitter_app'}'" });
      next LOOP_TWEET;

    } elsif (!defined($$user{'token'}) || !defined($$user{'secret'})) {
      update_tweet_status($tweet, { 'ret_resp' => "Invalid user '$$tweet{'twitter_account'}'" });
      next LOOP_TWEET;
    }

    # If debugging, flag and skip
    if ($config{'debug'} == 1) {
      log_msg('  In dry-run mode, not actually sending tweet (but would be just about to)');
      next LOOP_TWEET;
    }

    # Build our Twitter client
    my %ua = %{$config{'twitter'}{'user_agent'}};
    my $client_key = "$$tweet{'twitter_app'}::$$tweet{'twitter_account'}";
    $client{$client_key} = create_twitter_client($app, $user)
      if !defined($client{$client_key});
    my $r; my $c; my $e;

    # Determine if there is any media to upload / include
    @{$$tweet{'media'}} = ( );
    $media_sth->execute($$tweet{'app'}, $$tweet{'tweet_id'});
    while (my $media = $media_sth->fetchrow_hashref) {
      log_msg("  Requires media $$media{'media_id'}, with reference '$$media{'media_unique_ref'}'");

      # If we've already uploaded the media, add is upstream ID to the list to include
      if (defined($$media{'twitter_media_id'}) && !$$media{'media_expired'}) {
        log_msg("    Media already uploaded: remote ID '$$media{'twitter_media_id'}'), expires '$$media{'media_expires'}'");
        push @{$$tweet{'media'}}, $$media{'twitter_media_id'};
        next;
      } elsif (defined($$media{'twitter_media_id'})) {
        # Media was previously uploaded but has since expired, so flag this is the case and upload it again
        log_msg("    Media was already uploaded (remote ID '$$media{'twitter_media_id'}')) but has expired on '$$media{'media_expires'}' - re-attempting upload");
      }

      # Ensure the upload file is available
      if (! -e $$media{'media_local'}) {
        log_msg("    Local file '$$media{'media_local'}' cannot be found - unable to continue.");
        update_media_status($media, { 'ret_resp' => "Unable to upload media - local file not found" });
        next LOOP_TWEET;
      }

      # Perform the upload
      $e = undef;
      ($r, $c) = eval { $client{$client_key}->upload_media([$$media{'media_local'}], { 'media_category' => 'tweet_image' }) };
      $e = $@;

      # Process result
      my %ret = ( );
      # Media IDs and references
      try {
        $ret{'id'} = $r->{'media_id_string'};
        $ret{'key'} = $r->{'media_key'};
        $ret{'expires'} = $r->{'expires_after_secs'};
      } catch {
        log_msg("    There was an error parsing the media upload response for IDs");
      };
      log_msg("    Media ID: "  . (defined($ret{'id'}) ? "'$ret{'id'}'" : 'undef'));
      log_msg("    Media Key: " . (defined($ret{'key'}) ? "'$ret{'key'}'" : 'undef'));
      log_msg("    Expires: "   . (defined($ret{'expires'}) ? "'$ret{'expires'}'" : 'undef'));
      parse_response(\%ret, $r, $c, $e); # Glean what we can from the response objects
      update_media_status($media, \%ret); # Update status of this tweet

      # If no ID was returned, we cannot continue, so the whole tweet cannot be sent
      next LOOP_TWEET
        if !defined($ret{'id'});

      # Add this to the list to include in the tweet
      push @{$$tweet{'media'}}, $ret{'id'};
    }

    # Post or retweet?
    $e = undef;
    if (!$is_retweet) {
      # Do we need to include any media?
      my %args; %args = ( 'media_ids' => $$tweet{'media'} ) if @{$$tweet{'media'}};
      # Now send the Tweet
      ($r, $c) = eval { $client{$client_key}->update($$details{'tweet_body'}, \%args) };
    } else {
      ($r, $c) = eval { $client{$client_key}->retweet($$details{'twitter_tweet_id'}) };
    }
    $e = $@;

    # Process result
    my %ret = ( );
    # Tweet ID
    try {
      $ret{'id'} = $r->{'id_str'};
    } catch {
      log_msg("  There was an error parsing the tweet response for IDs");
    };
    log_msg("  Tweet ID: "  . (defined($ret{'id'}) ? "'$ret{'id'}'" : 'undef'));
    parse_response(\%ret, $r, $c, $e); # Glean what we can from the response objects
    update_tweet_status($tweet, \%ret); # Update status of this tweet
  }

# Log nothing found
} else {
  log_msg('No tweets queued');
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log();

##
## Interpret response objects
##
sub parse_response {
  my ($ret, $r, $c, $e) = @_;

  # Create a JSON encoding object, with blessed enabled (for Twitter::API::Context objects)
  my $json = JSON->new;
  $json->allow_blessed();

  # HTTP Return Code
  try {
    if (defined($c)) {
      $$ret{'ret_code'} = ${$c->http_response}{'_rc'};
    } elsif (defined($e) && ref $e ne 'SCALAR') {
      $$ret{'ret_code'} = ${$e->http_response}{'_rc'};
    } elsif (defined($e)) {
      $$ret{'ret_code'} = 500; # Assume Internal server error
    } else {
      die('Triggering catch...');
    }
  } catch {
    $$ret{'ret_code'} = undef;
  };
  log_msg('  HTTP Return Code: ' . (defined($$ret{'ret_code'}) ? "'$$ret{'ret_code'}'" : 'undef'));
  # Response
  try {
    if (defined($c)) {
      $$ret{'ret_resp'} = {'response' => $r, 'context' => $c};
    } elsif (defined($e) && ref $e ne 'SCALAR') {
      $$ret{'ret_resp'} = $e->twitter_error;
    } else {
      die('Triggering catch...');
    }
    $$ret{'ret_resp'} = $json->encode($$ret{'ret_resp'});
  } catch {
    $$ret{'ret_resp'} = undef;
  };
  # Info
  try {
    if (defined($c)) {
      $$ret{'ret_info'} = [ '_headers' => Dumper($c->http_response->headers),
                            '_request' => Dumper($c->http_response->request) ];
    } elsif (defined($e) && ref $e ne 'SCALAR') {
      $$ret{'ret_info'} = [ '_headers' => Dumper($e->http_response->headers),
                            '_request' => Dumper($e->http_response->request) ];
    } elsif (defined($e)) {
      $$ret{'ret_info'} = $e;
    } else {
      die('Triggering catch...');
    }
    $$ret{'ret_info'} = $json->encode($$ret{'ret_info'});
  } catch {
    $$ret{'ret_info'} = undef;
  };
}

##
## Write-back info from the Tweet request/response to the database
##
sub update_tweet_status {
  my ($tweet, $ret) = @_;

  # Validate $ret args
  foreach my $key ('id', 'ret_code', 'ret_resp', 'ret_info') {
    $$ret{$key} = undef if !exists $$ret{$key};
  }

  # Establish the send status
  my $status = (defined($$ret{'id'}) ? 'sent' : 'failed');
  log_msg("  Updating tweet with status '$status'");

  # Store the changes
  $tweet_update_sth->execute(
    $status,
    $$ret{'id'},
    $$ret{'ret_code'},
    $$ret{'ret_resp'},
    $$ret{'ret_info'},
    $$tweet{'app'},
    $$tweet{'tweet_id'}
  );
}

##
## Write-back info from the Media upload request/response to the database
##
sub update_media_status {
  my ($media, $ret) = @_;

  # Validate $ret args
  foreach my $key ('id', 'key', 'ret_code', 'ret_resp', 'ret_info') {
    $$ret{$key} = undef if !exists $$ret{$key};
  }

  # Store the changes
  $media_update_sth->execute(
    defined($$ret{'expires'}) ? 1 : 0, # Expiry time (in seconds) is set
    defined($$ret{'expires'}) ? $$ret{'expires'} : 0, # Value of expiry time
    $$ret{'id'},
    $$ret{'key'},
    $$ret{'ret_code'},
    $$ret{'ret_resp'},
    $$ret{'ret_info'},
    $$media{'app'},
    $$media{'tweet_id'},
    $$media{'media_id'}
  );

  # If we are storing a failed update (no ID element), update the linked tweet as we will not be able to proceed
  update_tweet_status($media, { 'ret_resp' => 'Unable to send tweet due to issue(s) processing its media' })
    if !defined($$ret{'id'});
}

##
## Import tweets
##
sub import_sync {
  #
  # Tweets
  #
  my $sync_rt_sth = $dbh->prepare('SELECT tweet_id FROM COMMS_TWITTER_SYNC WHERE app = ? AND sync_id = ?;');
  my $sync_ins_sth = $dbh->prepare('INSERT INTO COMMS_TWITTER (app, tweet_id, tweet_type, template_id, retweet_id, tweet_body, tweet_queued, tweet_send, tweet_status, twitter_app, twitter_account)
    VALUES (?, NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);');
  my $sync_upd_sth = $dbh->prepare('UPDATE COMMS_TWITTER_SYNC SET tweet_id = ? WHERE app = ? AND sync_id = ?;');
  my $sync_upd_media_sth = $dbh->prepare('UPDATE COMMS_TWITTER_SYNC_MEDIA SET tweet_id = ? WHERE app = ? AND sync_id = ?;');

  my $sql = 'SELECT app, sync_id, tweet_type, template_id, retweet_id, tweet_body, tweet_queued, tweet_send, tweet_status, twitter_app, twitter_account
    FROM COMMS_TWITTER_SYNC WHERE tweet_id IS NULL ORDER BY app, sync_id;';
  my @sync = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };
  if (@sync) {
    log_msg('Importing ' . scalar(@sync) . ' message(s) to the queue');
    foreach my $tweet (@sync) {
      my $is_retweet = defined($$tweet{'retweet_id'});
      log_msg("  App: $$tweet{'app'}; Sync ID: $$tweet{'sync_id'}; Is Retweet: " . ($is_retweet ? "Yes (Sync $$tweet{'retweet_id'})" : 'No') . " (from app $$tweet{'twitter_app'}, user $$tweet{'twitter_account'})");

      # If we're retweeting, convert the Retweet ID from sync to queue
      if ($is_retweet) {
        my $sync_id = $$tweet{'retweet_id'};
        $sync_rt_sth->execute($$tweet{'app'}, $sync_id);
        ($$tweet{'retweet_id'}) = $sync_rt_sth->fetchrow_array;
        log_msg("    Converted Retweet ID from sync $sync_id to imported $$tweet{'retweet_id'}");
      }

      # Add to the main queue table
      $sync_ins_sth->execute(
        $$tweet{'app'},
        $$tweet{'tweet_type'},
        $$tweet{'template_id'},
        $$tweet{'retweet_id'},
        $$tweet{'tweet_body'},
        $$tweet{'tweet_queued'},
        $$tweet{'tweet_send'},
        $$tweet{'tweet_status'},
        $$tweet{'twitter_app'},
        $$tweet{'twitter_account'}
      );
      # And write back the ID to confirm import
      my $tweet_id = $dbh->last_insert_id(undef, undef, undef, undef);
      $sync_upd_sth->execute($tweet_id, $$tweet{'app'}, $$tweet{'sync_id'});
      $sync_upd_media_sth->execute($tweet_id, $$tweet{'app'}, $$tweet{'sync_id'});
      log_msg("    Imported with ID $tweet_id");
    }
  }

  #
  # Media
  #
  $sync_ins_sth = $dbh->prepare('INSERT INTO COMMS_TWITTER_MEDIA (app, tweet_id, media_id, media_local, media_unique_ref, media_order)
    VALUES (?, ?, NULL, ?, ?, ?);');
  $sync_upd_sth = $dbh->prepare('UPDATE COMMS_TWITTER_SYNC_MEDIA SET media_id = ? WHERE app = ? AND sync_id = ? AND sync_media_id = ?;');

  $sql = 'SELECT app, sync_id, sync_media_id, tweet_id, media_local, media_unique_ref, media_order
    FROM COMMS_TWITTER_SYNC_MEDIA WHERE media_id IS NULL ORDER BY app, sync_id, sync_media_id;';
  @sync = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };
  if (@sync) {
    log_msg('Importing ' . scalar(@sync) . ' media(s) to the queue');
    foreach my $media (@sync) {
      log_msg("  App: $$media{'app'}; Sync ID: $$media{'sync_id'} (Tweet ID: $$media{'tweet_id'}); File: $$media{'media_local'}; Order: $$media{'media_order'}");

      # Add to the main queue table
      $sync_ins_sth->execute(
        $$media{'app'},
        $$media{'tweet_id'},
        $$media{'media_local'},
        $$media{'media_unique_ref'},
        $$media{'media_order'}
      );
      # And write back the ID to confirm import
      my $media_id = $dbh->last_insert_id(undef, undef, undef, undef);
      $sync_upd_sth->execute($media_id, $$media{'app'}, $$media{'sync_id'}, $$media{'sync_media_id'});
      log_msg("    Imported with ID $media_id");
    }
  }
}
