#!/usr/bin/perl -w
# Perl config file

our %config;
require $base_dir . '/../config.pl';

# Errors to log but not email
$config{'ignore_warnings'} = [ ];

# Log message
my $lf = basename($0); $lf =~ s/\.pl$//;
$config{'log_file'} = { 'dir' => $base_dir . '/../../logs/common/comms-' . $lf,
                        'fmt' => '%Y-%m-%d',
                        'ext' => 'log.gz' };
$config{'log'} = '';

##
## Sub-routines
##
#
# Establish where to save the log
#
sub setup_logging {
  my $log_date = strftime($config{'log_file'}{'fmt'}, localtime);
  $config{'log_file'} = $config{'log_file'}{'dir'}. '/' . $log_date . '.' . $config{'log_file'}{'ext'};
  $config{'log'} = '==========' . "\n";
}

#
# Die (and log reason...) if an error occurred
#
sub die_on_error {
  my ($err) = @_;
  log_msg('ERROR - ' . $err);
  push @errors, 'E - ' . $err;
  save_log();
  exit;
}

#
# Warn (and log reason...) if an error occurred, allowing is to continue
#
sub warn_on_error {
  my ($err) = @_;
  log_msg('WARNING - ' . $err);
  $warn_flag = 1;
}

#
# Save a debug message
#
sub debug {
  my ($msg) = @_;
  log_msg($msg) if $opt{'debug'} || $opt{'verbose'};
}
sub verbose {
  my ($msg) = @_;
  log_msg($msg) if $opt{'verbose'};
}

#
# Check to see if a warning is common enough that we do not want to email it
#
sub ignore_warning {
  my ($err) = @_;

  # Loop through the list and look for matches
  foreach (@{$config{'ignore_warnings'}}) {
    return 1 if $err =~ /$_/si;
  }

  # Not in the list, so not one we are ignoring
  return 0;
#  grep(/$e/, @{$config{'ignore_warnings'}})
}

#
# Record a log message with a timestamp
#
sub log_msg {
  my ($msg) = @_;
  $config{'log'} .= strftime('%Y-%m-%d %H:%M:%S', localtime) . ' - ' . $msg . "\n";
}

#
# Save the log to file
#
sub save_log {
  # If we're displaying to STDOUT instead, do so
  if ($opt{'no-save-log'} || $opt{'debug'} || $opt{'verbose'}) {
    print $config{'log'};
    return if $opt{'no-save-log'};
  }

  # The saving...
  output_file($config{'log_file'}, $config{'log'});

  # If we have errors, email them to the Support Team
  send_email_queued('bin_comms', 'error',
                    $0 . ' error' . (scalar(@errors) == 1 ? '' : 's'),
                    'The following error' . (scalar(@errors) == 1 ? '' : 's') . ' occurred during the current run:' . "\n\n" . join("\n", @errors))
    if ($config{'live_server'} && scalar(@errors));
}

# Return true to pacify the compiler
1;
