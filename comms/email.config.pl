#!/usr/bin/perl -w
# Email config

##
## General config
##
our %config;
$config{'debug'} = 0; # Three state debug flag: 0 = live, 1 = dry run, 2 = run with test credentials
$config{'email'}{'x-mailer'} = 'DeBear Mailer/1.0';

# Return true to pacify the compiler
1;
