#!/usr/bin/perl -w
# Send all queued emails marked to be sent

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
  }
}

use strict;
use utf8;
use DBI;
use HTML::Entities;

# Mask the filename as we're on a shared server
our $base_dir;
$0 = basename $0;

# Get base config
our %config;
require $base_dir . '/config.pl';
require $base_dir . '/email.config.pl';

# Logging info
setup_logging();

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
$dbh->do('SET time_zone = "Europe/London";')
  or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);

# Get the list of emails to send
my $sql = 'SELECT app, email_id, email_from, email_to, email_subject, email_body FROM COMMS_EMAIL WHERE email_send <= NOW() AND email_status = "queued" ORDER BY email_send;';
my @queue = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };
my $update_sth = $dbh->prepare('UPDATE COMMS_EMAIL SET email_sent = NOW(), email_status = ? WHERE app = ? AND email_id = ?;');

# Loop through, sending
if (@queue) {
  foreach my $email (@queue) {
    log_msg("Sending email $$email{'app'} / $$email{'email_id'} ($$email{'email_to'}, $$email{'email_subject'})");

    # Decode database content
    foreach my $key ('to', 'from', 'subject', 'body') {
      $$email{'email_' . $key} = decode_entities($$email{'email_' . $key});
    }

    # Switch the email_to field to a "safe" account if debugging
    if ($config{'debug'} == 2) {
      $$email{'email_to'} = 'support@debear.uk';
      log_msg("- In test-credential debug mode, switching email_to to '$$email{'email_to'}'");
    }

    # Get our other headers?
    my @headers = ( );
    my ($boundary) = ($$email{'email_body'} =~ m/^--(\w{16})/i);
    my $is_html = defined($boundary);
    my $content_type = ($is_html
                         ? 'multipart/alternative; boundary="' . $boundary . '"'
                         : 'text/plain');

    push @headers, 'MIME-Version: 1.0';
    push @headers, 'Content-type: ' . $content_type . '; charset=UTF-8';
    push @headers, 'Content-Transfer-Encoding: 7bit';

    # Form our email
    push @headers, 'X-Mailer: ' . $config{'email'}{'x-mailer'};

    # If debugging, flag and skip
    if ($config{'debug'} == 1) {
      log_msg('- In dry-run mode, not actually sending email (but would be just about to)');
      next;
    }

    # Send and update
    my $success = send_email_sendmail($$email{'email_from'}, $$email{'email_to'}, $$email{'email_subject'}, $$email{'email_body'}, join("\n", @headers));
    update_status($email, $success);
  }

  # TODO: Update status of email storer (e.g., Fantasy_Status) when sent?

# Log nothing found
} else {
  log_msg('No emails queued');
}

# Disconnect from the database
$update_sth->finish if defined($update_sth);
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log();

##
## Sub-routines
##
sub update_status {
  my ($email, $success) = @_;

  # Convert the $success bool to a status
  my $status = ($success ? 'sent' : 'failed');
  log_msg("Updating email with status '$status'");

  # Store the changes
  $update_sth->execute($status, $$email{'app'}, $$email{'email_id'});
}

#
# Send email via sendmail
#
sub send_email_sendmail {
  my ($from, $to, $subject, $body, $headers) = @_;
  my $msg = 'From: ' . $from . "\n" .
            'To: ' . $to . "\n" .
            'Subject: ' . $subject . "\n" .
            $headers . "\n\n" .
            $body . "\n";
  open MAIL, '|' . $config{'email'}{'sendmail'};
  binmode MAIL, ":utf8";
  print MAIL $msg . "\n";
  return close MAIL;
}

