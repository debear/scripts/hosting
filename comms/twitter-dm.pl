#!/usr/bin/perl -w
# Send all queued twitter direct message's marked to be sent

#
# Update @INC on the Xilo server
#
BEGIN {
  use File::Basename;

  # Supress warnings from STDERR
  $SIG{'__WARN__'} = sub { };

  # Some path customisation needs to be made if running on the prod server
  my $xilo_home = '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116';
  my $xilo_custom_path = 'debear/sites/common/perl5'; # Relative to ~
  our $xilo_is = 0;
  our $base_dir = dirname $0;
  if ($base_dir =~ /^$xilo_home/) {
    # Okay, now link to the other perl modules
    unshift(@INC, $xilo_home.'/'.$xilo_custom_path);
    $xilo_is = 1;
  } else {
    # Non-standard: use our customised Net::Twitter::Lite::WithAPIv1_1 on dev too...
    unshift(@INC, '/var/www/debear/sites/common/perl5');
  }
}

use strict;
use utf8;
use DBI;
use Twitter::API;
use Scalar::Util qw(blessed);
use HTML::Entities;
use JSON;
use Try::Tiny;
use Data::Dumper; # Not added for testing, used for dumping return info

# Mask the filename as we're on a shared server
our $base_dir;
$0 = basename $0;

# Get base config, plus our list of Twitter accounts
our %config; our $xilo_is;
require $base_dir . '/config.pl';
require $base_dir . '/twitter.config.pl';

# Logging info
setup_logging();

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db'}{'name'} . ':' . $config{'db'}{'host'}, $config{'db'}{'user'}, $config{'db'}{'pass'})
  or die_on_error('Unable to connect: ' . $DBI::errstr);
$dbh->do('SET time_zone = "Europe/London";')
  or die_on_error('Unable to enforce timezone: ' . $DBI::errstr);

# Prepare the SQL statements
my $update_sth = $dbh->prepare('UPDATE COMMS_TWITTER_DM SET dm_sent = NOW(), dm_status = ?, twitter_return_code = ?, twitter_return_response = COMPRESS(?), twitter_return_info = COMPRESS(?) WHERE app = ? AND dm_id = ?;');

# Get the list of messages to send
my $sql = 'SELECT app, dm_id, recipient_id, dm_body, twitter_app, twitter_account FROM COMMS_TWITTER_DM WHERE dm_send <= NOW() AND dm_status = "queued" ORDER BY dm_send, app, dm_id;';
my @queue = @{ $dbh->selectall_arrayref($sql, { Slice => {} }) };

# Twitter clients
my %client = ( );

# Loop through, sending
if (@queue) {
  foreach my $dm (@queue) {
    log_msg("Sending direct message $$dm{'app'} / $$dm{'dm_id'} (from app $$dm{'twitter_app'}, user $$dm{'twitter_account'})");

    # Decode the encoded body
    $$dm{'dm_body'} = decode_entities($$dm{'dm_body'});

    # If testing, use alternate user details
    if (!$xilo_is || ($config{'debug'} == 2)) {
      $$dm{'twitter_app'} = $config{'twitter'}{'test'}{'app'};
      $$dm{'twitter_account'} = $config{'twitter'}{'test'}{'account_pri'};
      $$dm{'recipient_id'} = $config{'twitter'}{'test'}{'account_sec_int'};
      log_msg("- In test-credential debug mode, switching app to '$$dm{'twitter_app'}' and '$$dm{'twitter_account'}' and recipient to '$$dm{'recipient_id'}'");
    }

    # Then the keys and tokens
    my $app = load_key($$dm{'twitter_app'}, '_app');
    my $user = load_key($$dm{'twitter_app'}, $$dm{'twitter_account'});

    # Validate these keys and tokens, throw error and skip if invalid
    if (!defined($$app{'key'}) || !defined($$app{'secret'})) {
      update_status($dm, { 'ret_resp' => "Invalid app '$$dm{'twitter_app'}'" });
      next;

    } elsif (!defined($$user{'token'}) || !defined($$user{'secret'})) {
      update_status($dm, { 'ret_resp' => "Invalid user '$$dm{'twitter_account'}'" });
      next;
    }

    # If debugging, flag and skip
    if ($config{'debug'} == 1) {
      log_msg('- In dry-run mode, not actually sending direct message (but would be just about to)');
      next;
    }

    # Send the tweet
    my %ua = %{$config{'twitter'}{'user_agent'}};
    my $client_key = "$$dm{'twitter_app'}::$$dm{'twitter_account'}";
    $client{$client_key} = create_twitter_client($app, $user)
      if !defined($client{$client_key});

    # Send the message
    my ($r, $c) = eval { $client{$client_key}->new_direct_messages_event({
      'type' => 'message_create',
      'message_create' => {
        'target' => {
          'recipient_id' => $$dm{'recipient_id'},
        },
        'message_data' => {
          'text' => $$dm{'dm_body'},
        }
      }
    }); } or die "Unable to send Twitter DM: $@";
    my $e = $@;

    # Process result
    my %ret = ( );
    # Message ID
    try {
      $ret{'id'} = $r->{'event'}{'id'};
      log_msg("Message ID: '$ret{'id'}'");
    } catch {
      $ret{'id'} = undef;
      log_msg("Message ID: undef");
    };

    # Create a JSON encoding object, with blessed enabled (for Twitter::API::Context objects)
    my $json = JSON->new;
    $json->allow_blessed();

    # HTTP Return Code
    try {
      if (defined($c)) {
        $ret{'ret_code'} = ${$c->http_response}{'_rc'};
      } elsif (defined($e) && ref $e ne 'SCALAR') {
        $ret{'ret_code'} = ${$e->http_response}{'_rc'};
      } elsif (defined($e)) {
        $ret{'ret_code'} = 500; # Assume Internal server error
      } else {
        die('Triggering catch...');
      }
      log_msg("HTTP Return Code: '$ret{'ret_code'}'");
    } catch {
      $ret{'ret_code'} = undef;
      log_msg("HTTP Return Code: undef");
    };
    # Response
    try {
      if (defined($c)) {
        $ret{'ret_resp'} = {'response' => $r, 'context' => $c};
      } elsif (defined($e) && ref $e ne 'SCALAR') {
        $ret{'ret_resp'} = $e->twitter_error;
      } else {
        die('Triggering catch...');
      }
      $ret{'ret_resp'} = $json->encode($ret{'ret_resp'});
    } catch {
      $ret{'ret_resp'} = undef;
    };
    # Info
    try {
      if (defined($r)) {
        $ret{'ret_info'} = [ '_headers' => Dumper($c->http_response->headers),
                             '_request' => Dumper($c->http_response->request) ];
      } elsif (defined($e) && ref $e ne 'SCALAR') {
        $ret{'ret_info'} = [ '_headers' => Dumper($e->http_response->headers),
                             '_request' => Dumper($e->http_response->request) ];
      } elsif (defined($e)) {
        $ret{'ret_info'} = $e;
      } else {
        die('Triggering catch...');
      }
      $ret{'ret_info'} = $json->encode($ret{'ret_info'});
    } catch {
      $ret{'ret_info'} = undef;
    };

    # Update status of this tweet
    update_status($dm, \%ret);
  }

# Log nothing found
} else {
  log_msg('No messages queued');
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);

# Save the log to file and then exit
save_log();

##
## Sub-routines
##
sub update_status {
  my ($dm, $ret) = @_;

  # Validate $ret args
  foreach my $key ('ret_code', 'ret_resp', 'ret_info') {
    $$ret{$key} = undef if !exists $$ret{$key};
  }

  # Establish the send status
  my $status = (defined($$ret{'id'}) ? 'sent' : 'failed');
  log_msg("- Updating direct message with status '$status'");

  # Store the changes
  $update_sth->execute(
    $status,
    $$ret{'ret_code'},
    $$ret{'ret_resp'},
    $$ret{'ret_info'},
    $$dm{'app'},
    $$dm{'dm_id'}
  );
}
